﻿using System;
using System.Collections.Generic;
using System.Linq;

using SimpleCSharp.Exceptions;
using SimpleCSharp.Models.Tree;

namespace SimpleCSharp.Parser
{
    public partial class Parser
    {
        private StmtLst GetStmtLst()
        {
            var stm = this.GetStmt();
            if (stm == null)
                return null;

            var lst = new StmtLst(stm.Line, stm.Position);
            lst.Children.Add(stm);

            while ((stm = this.GetStmt()) != null)
            {
                lst.Children.Add(stm);
            }

            return lst;
        }
    }
}
