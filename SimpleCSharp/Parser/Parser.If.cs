﻿using System;
using System.Collections.Generic;
using System.Linq;

using SimpleCSharp.Exceptions;
using SimpleCSharp.Models.Tree;

namespace SimpleCSharp.Parser
{
    public partial class Parser
    {
        private If GetIf()
        {
            var cursor = this.textAnalyzer.Cursor;//to rollback

            this.textAnalyzer.SkipEmptyChars();
            var position = this.textAnalyzer.Cursor;
            var line = this.GetLine(position);

            bool ifWord = this.textAnalyzer.TryRead("if");
            if (!ifWord)
            {
                //no cursor movement, no rollback
                return null;
            }

            string name = this.GetName();
            if (name == null)
            {
                this.textAnalyzer.Cursor = cursor;//rollback
                return null;
            }
            Logger.Debug($"if var name: {name}");

            this.textAnalyzer.SkipEmptyChars();
            bool thenWord = this.textAnalyzer.TryRead("then");
            if (!thenWord)
            {
                throw new ExpectedException("then", this.textAnalyzer.Cursor, GetActualLine());
            }

            this.textAnalyzer.SkipEmptyChars();
            bool boChar = this.textAnalyzer.TryRead("{");
            if (!boChar)
            {
                throw new ExpectedException("{", this.textAnalyzer.Cursor, GetActualLine());
            }

            var stmtNo = GetNextStmtNo();

            var ifTrue = this.GetStmtLst();
            if (ifTrue == null)
            {
                throw new StmtLstExpectedException(this.textAnalyzer.Cursor, GetActualLine());
            }

            this.textAnalyzer.SkipEmptyChars();
            bool bcChar = this.textAnalyzer.TryRead("}");
            if (!bcChar)
            {
                throw new ExpectedException("}", this.textAnalyzer.Cursor, GetActualLine());
            }

            var ifResult = new If(line, position, stmtNo)
            {
                Condition = new NameVal(line, position)
                {
                    Value = name
                },
                IfTrue = ifTrue
            };

            this.textAnalyzer.SkipEmptyChars();
            bool elseWord = this.textAnalyzer.TryRead("else");
            if (!elseWord)
            {
                return ifResult;
            }

            this.textAnalyzer.SkipEmptyChars();
            boChar = this.textAnalyzer.TryRead("{");
            if (!boChar)
            {
                throw new ExpectedException("{", this.textAnalyzer.Cursor, GetActualLine());
            }

            var ifFalse = this.GetStmtLst();
            if (ifFalse == null)
            {
                throw new StmtLstExpectedException(this.textAnalyzer.Cursor, GetActualLine());
            }

            this.textAnalyzer.SkipEmptyChars();
            bcChar = this.textAnalyzer.TryRead("}");
            if (!bcChar)
            {
                throw new ExpectedException("}", this.textAnalyzer.Cursor, GetActualLine());
            }

            ifResult.IfFalse = ifFalse;

            return ifResult;
        }
    }
}
