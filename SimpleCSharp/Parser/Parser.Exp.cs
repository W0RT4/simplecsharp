﻿using System;
using System.Collections.Generic;
using System.Linq;

using SimpleCSharp.Models.Tree;

namespace SimpleCSharp.Parser
{
    public partial class Parser
    {


        private FactorVal GetFactor()
        {
            var cursor = this.textAnalyzer.Cursor;//to rollback
            var position = this.textAnalyzer.Cursor;


            int? number = null;
            string name = this.GetName();
            if (name == null)
            {
                number = this.GetNumber();
                if (number == null)
                {
                    if (this.textAnalyzer.TryRead("("))
                    {
                        var exp = this.GetExp();
                        if (exp != null)
                        {
                            if (!this.textAnalyzer.TryRead(")"))
                            {
                                this.textAnalyzer.Cursor = cursor;
                                return null;
                            }
                            return exp;
                        }

                    }

                    this.textAnalyzer.Cursor = cursor;
                    return null;
                }
            }
            return new FactorVal(this.textAnalyzer.Cursor, this.GetLine(this.textAnalyzer.Cursor))
            {
                Value = name ?? number.Value.ToString()
            };
        }


        struct ExpElement
        {
            public FactorVal Factor { get; set; }
            public string Operator { get; set; }

            public ExpElement(FactorVal factor, string _operator)
            {
                this.Factor = factor;
                this.Operator = _operator;
            }
        }

        private List<ExpElement> GetExpElements()
        {
            var result = new List<ExpElement>();

            var cursor = this.textAnalyzer.Cursor;//to rollback
            var position = this.textAnalyzer.Cursor;
            var line = this.GetLine(position);


            var factor = GetFactor();
            if (factor == null)
            {
                this.textAnalyzer.Cursor = cursor;//rollback to be sure
                return null;
            }
            result.Add(new ExpElement(factor, null));

            while (true)
            {
                this.textAnalyzer.SkipEmptyChars();
                var operators = new string[] { "+", "*", "-", "/" };//TODO: move to const
                string oper = null;
                foreach (var op in operators)
                {
                    if (!this.textAnalyzer.TryRead(op))
                    {
                        continue;
                    }

                    oper = op;
                    break;
                }
                if (oper == null)//no known mathematical operator was found
                {
                    return result;
                }

                result.Add(new ExpElement(null, oper));


                var factor2 = GetFactor();
                if (factor2 == null)
                {
                    this.textAnalyzer.Cursor = cursor;//rollback to be sure
                    return null;
                }
                result.Add(new ExpElement(factor2, null));
            }

            return result;
        }



        private FactorVal GetExp()
        {
            var cursor = this.textAnalyzer.Cursor;//to rollback

            var position = this.textAnalyzer.Cursor;
            var line = this.GetLine(position);

            var expElements = GetExpElements();

            if (expElements == null)
            {
                return null;
            }

            if (expElements.Count == 1)
            {
                return expElements[0].Factor;
            }


            var result = new Exp(line, position);
            var work = result;


            while (true)
            {


                if (expElements[1].Operator == "-" || expElements[1].Operator == "+")
                {
                    work.Value = expElements[1].Operator;
                    work.Children.Add(expElements[0].Factor);

                    expElements.RemoveAt(0); expElements.RemoveAt(0);

                    if (expElements.Count == 1)
                    {
                        work.Children.Add(expElements[0].Factor);
                        return result;
                    }

                    var newWork = new Exp(line, position);
                    work.Children.Add(newWork);
                    work = newWork;
                }
                else if (expElements[1].Operator == "/" || expElements[1].Operator == "*")
                {
                    if (expElements.Count <= 3) // 2*2
                    {
                        work.Value = expElements[1].Operator;
                        work.Children.Add(expElements[0].Factor);
                        work.Children.Add(expElements[2].Factor);

                        return result;
                    }
                    else // 1*2*3
                    if (expElements[3].Operator == "/" || expElements[3].Operator == "*")
                    {
                        work.Value = expElements[1].Operator;
                        work.Children.Add(expElements[0].Factor);
                        expElements.RemoveAt(0); expElements.RemoveAt(0);

                        var newWork = new Exp(line, position);
                        work.Children.Add(newWork);
                        work = newWork;
                    }
                    else //1*2+3
                    if (expElements[3].Operator == "-" || expElements[3].Operator == "+")
                    {
                        work.Value = expElements[3].Operator;

                        var leftChild = new Exp(line, position);
                        leftChild.Value = expElements[1].Operator;
                        leftChild.Children.Add(expElements[0].Factor);
                        leftChild.Children.Add(expElements[2].Factor);
                        work.Children.Add(leftChild);

                        expElements.RemoveAt(0); expElements.RemoveAt(0); expElements.RemoveAt(0); expElements.RemoveAt(0);

                        if (expElements.Count == 1)
                        {
                            work.Children.Add(expElements[0].Factor);
                            return result;
                        }

                        var newWork = new Exp(line, position);
                        work.Children.Add(newWork);

                        work = newWork;
                    }
                    else
                    {
                        throw new Exception("line: " + line + " panic error!");
                    }
                }
            }


            return result;
        }
    }
}
