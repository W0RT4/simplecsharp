﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCSharp.QueryProcessor
{
    public class QueryProcessorException : Exception
    {
        public QueryProcessorException() : base()
        {
        }

        public QueryProcessorException(string message) : base(message)
        {
        }

        public QueryProcessorException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
