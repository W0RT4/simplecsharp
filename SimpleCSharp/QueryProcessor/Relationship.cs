﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCSharp.QueryProcessor
{
    class Relationship
    {
        public int numOfArgs { get; }
        public List<string> args1 { get; }
        public List<string> args2 { get; }
        public Relationship(int num,List<string> arg1,List<string> arg2)
        {
            this.numOfArgs = num;

            this.args1 = arg1;
            this.args2 = arg2;
        }
    }
}
