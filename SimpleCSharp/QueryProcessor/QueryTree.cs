﻿using System;
using System.Collections.Generic;
namespace SimpleCSharp.QueryProcessor
{
    public class QueryTree
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(typeof(QueryTree));
        const string NO_RELATIONSHIP = "";
        const string SELECT_RELATIONSHIP = "select";
        public PatternArgs p = new PatternArgs();
        public List<Clause> synonymsList { get; } = new List<Clause>();
        public List<Clause> clausesList { get; } = new List<Clause>();
        public Clause selectClause { get; private set; }

        public QueryTree()
        {

        }
        public void InsertSynonym(string var, string varType)
        {
            synonymsList.Add(new Clause(NO_RELATIONSHIP, new List<string> { var }, new List<string> { varType }));
        }

        public void InsertSelect(List<string> varList, List<string> varTypeList)
        {
            selectClause = new Clause(SELECT_RELATIONSHIP, varList, varTypeList);
        }

        public void InsertSuchThat(string rel, List<string> varList, List<string> varTypeList)
        {
            clausesList.Add(new Clause(rel, varList, varTypeList));
        }

        public void InsertPattern(string rel, List<string> varList, List<string> varTypeList)
        {
            clausesList.Add(new Clause(rel, varList, varTypeList));
        }

        public void InsertWith(string rel, List<string> varList, List<string> varTypeList)
        {
            clausesList.Add(new Clause(rel, varList, varTypeList));
        }
    }
}