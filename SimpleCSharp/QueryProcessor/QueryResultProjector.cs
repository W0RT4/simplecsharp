﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCSharp.QueryProcessor
{
    class QueryResultProjector
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(typeof(QueryResultProjector));
        private const string NO_RESULT = "none";
        private const string NO_RELATIONSHIP = "";
        private const string TRUE_STRING = "true";
        private const string FALSE_STRING = "false";

        private QueryRawResult rawResult;
        private Clause selectClause;
        private QueryValidator queryValidator;
        public QueryResultProjector(QueryRawResult qrr, Clause selectClause, QueryValidator qv)
        {
            this.rawResult = qrr;
            this.selectClause = selectClause;
            this.queryValidator = qv;
        }

        public String getCommaSeparatedResult()
        {
            if (selectClause.var[0].Equals(NO_RELATIONSHIP))
            {
                if (rawResult.isQueryTrue)
                {
                    return TRUE_STRING;
                }
                return FALSE_STRING;
            }
            List<int> hashmap = new List<int>();
            int index = -1;
            for (int i = 0; i < rawResult.header.Count; i++)
            {
                if (rawResult.header[i].Equals(selectClause.var[0]))
                {
                    index = i;
                    break;
                }
            }
            for (int i = 0; i < rawResult.result.Count; i++)
            {
                hashmap.Add(rawResult.result[i].ElementAt(index));
            }
            hashmap = hashmap.Distinct().ToList();
            hashmap.Remove(0);

            String tmp = NO_RESULT;
            if (hashmap.Count == 0 && rawResult.isQueryTrue)
            {
                hashmap = PKB.GetEnties(queryValidator.getVarType(selectClause.var[0])).Select(o => o.Line).ToList();
            }
            if (rawResult.isQueryTrue && queryValidator.getVarType(selectClause.var[0]).Equals("procedure"))
            {
                tmp = "";
                for (int i = 0; i < hashmap.Count; i++)
                {
                    tmp += "," + PKB.GetProcedure(hashmap[i]);
                }
                tmp = tmp.Substring(1);
            }
            else
            {
                tmp = String.Join(",", hashmap);
            }
            return tmp.Equals("") ? NO_RESULT : tmp;
        }
    }
}
