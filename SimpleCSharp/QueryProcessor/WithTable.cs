﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCSharp.QueryProcessor
{
    class WithTable
    {
        public Dictionary<string, With> witTable = new Dictionary<string, With>();

        public WithTable()
        {
            List<string> arg1;
            List<string> arg2;
            arg1 = new List<string>() { "string", "all" };
            arg2 = new List<string>() { "string", "all" };
            With wit1 = new With(arg1, arg2);
            witTable["with"] = wit1;
        }
    }
}
