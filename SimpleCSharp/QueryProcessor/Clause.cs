﻿using System;
using System.Collections.Generic;
namespace SimpleCSharp.QueryProcessor
{
    public class Clause
    {
        public string relationship { get; set; }
        public List<string> var { get; set; }
        public List<string> varType { get; set; }
        public int numOfArgs { get; set; }


        public Clause() { }
        public Clause(string _rel, List<string> _var, List<string> _varType)
        {
            this.relationship = _rel;
            this.var = _var;
            this.varType = _varType;
        }
    }
}