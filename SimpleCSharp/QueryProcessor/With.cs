﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCSharp.QueryProcessor
{
    class With
    {
        public List<string> args1 { get; }
        public List<string> args2 { get; }
        public With(List<string> arg1, List<string> arg2)
        {
            this.args1 = arg1;
            this.args2 = arg2;
        }
    }
}
