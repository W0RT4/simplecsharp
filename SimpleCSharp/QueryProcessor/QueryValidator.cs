﻿using System;
using System.IO;
using SimpleCSharp.QueryProcessor.Exceptions;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace SimpleCSharp.QueryProcessor
{

    public class QueryValidator
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(typeof(QueryValidator));

        const string NO_RELATIONSHIP = "";

        const string VARTYPE_VARIABLE = "variable";
        const string VARTYPE_STRING = "string";
        const string VARTYPE_CONSTANT = "constant";
        const string VARTYPE_ASSIGN = "assign";
        const string VARTYPE_WHILE = "while";
        const string VARTYPE_STMT = "stmt";
        const string VARTYPE_IF = "if";
        const string VARTYPE_PROCNAME = "procName";
        const string VARTYPE_VARNAME = "varName";
        const string VARTYPE_PROG_LINE = "prog_line";
        const string VARTYPE_NUMBER = "number";
        const string VARTYPE_BOOLEAN = "BOOLEAN";
        const string VARTYPE_ALL = "all";
        const string RELTYPE_AND = "and";
        const string RELTYPE_SELECT = "Select";
        const string RELTYPE_SUCHTHAT = "such that";
        const string RELTYPE_WITH = "with";
        const string RELTYPE_WITH_NAME = "WithName";
        const string RELTYPE_WITH_NUMBER = "WithNumber";
        const string RELTYPE_PATTERN = "pattern";
        const string REL_MODIFIES = "Modifies";
        const string REL_USES = "Uses";

        private static string[] relationships ={
            REL_MODIFIES,
            REL_USES,
            "Calls",
            "Calls*",
            "Parent",
            "Parent*",
            "Follows",
            "Follows*",
            "Next",
            "Next*"
        };
        private static string[] designEntites = {
            "procedure",
            "stmt",
            "stmtLst",
            "while",
            "assign",
            "if",
            "call",
            "variable",
            "constant",
            "prog_line"
        };
        private Dictionary<string, string> varMap = new Dictionary<string, string>();
        private RelationshipTable relTable = new RelationshipTable();


        public QueryTree queryTree { get; }

        public QueryValidator()
        {
            queryTree = new QueryTree();
        }

        public bool isSynonymAndQueryValid(string synonyms, string query)
        {
            if (!isSynonymValid(synonyms))
            {
                throw new SynonymsInvalidException();
            }
            if (!isQueryValid(query))
            {

                throw new QueryInvalidException();
            }
            return true;
        }

        private bool isSynonymValid(string synonyms)
        {
            synonyms.Trim();
            if (synonyms.Equals("")) { return true; }
            bool checker = Regex.Match(synonyms, "^[a-z]{1}[_a-zA-Z0-9 ,;]{4,}$").Success;
            if (!checker) { return false; }
            string[] vars = synonyms.Trim().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < vars.Length; i++)
            {
                string[] syms = vars[i].Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (syms.Length < 2) { return false; }
                string[] vNames = syms[1].Trim().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (!isValidDesignEntity(syms[0])) { return false; }
                for (int j = 0; j < vNames.Length; j++)
                {
                    if (isVarNameExists(vNames[j]))
                    {
                        return false;
                    }
                    queryTree.InsertSynonym(vNames[j], syms[0]);
                    varMap.Add(vNames[j], syms[0]);
                }
            }
            return true;
        }
        private bool isQueryValid(string query)
        {
            string temp = "";
            string temp1 = "";
            string temp2 = "";
            bool checker = Regex.Match(query, "^[a-zA-Z0-9 \\.,=#\\(\\)<>_\"\\*]{8,}").Success;
            if (!checker) { return false; }
            TextAnalyzer textAn = new TextAnalyzer();
            textAn.Text = query;
            if (textAn.TryRead(RELTYPE_SELECT))
            {
                textAn.SkipEmptyChars();
                if (textAn.TryRead(VARTYPE_BOOLEAN))
                {
                    queryTree.InsertSelect(new List<string>() { NO_RELATIONSHIP }, new List<string>() { VARTYPE_BOOLEAN });
                }
                else
                {
                    temp = textAn.TryRead(new Regex("[a-zA-Z0-9,<>]+")).Trim();
                    if (!isVarNameExists(temp)) { return false; }
                    queryTree.InsertSelect(new List<string>() { temp }, new List<string>() { varMap[temp] });
                    if (textAn.IsEndText()) { return true; }
                }
                textAn.SkipEmptyChars();

                while (!textAn.IsEndText())
                {
                    textAn.SkipEmptyChars();
                    if (textAn.IsEndText()) { break; }
                    textAn.TryRead(RELTYPE_AND);
                    textAn.SkipEmptyChars();
                    if (textAn.TryRead(RELTYPE_SUCHTHAT))
                    {
                        textAn.SkipEmptyChars();
                        temp = textAn.TryRead(new Regex("[A-Z]{1}[a-z\\*]+[ ]*\\([a-zA-Z0-9_ \"]+,[a-zA-Z0-9 _\"]+\\)")).Trim();
                        if (temp.Equals("")) { return false; }
                        if (!parseRelationship(temp)) { return false; }
                    }
                    else if (textAn.TryRead(RELTYPE_PATTERN))
                    {
                        textAn.SkipEmptyChars();
                        temp1 = textAn.TryRead(new Regex("[a-zA-Z]{1}[a-zA-Z0-9 ]*")).Trim();
                        temp2 = textAn.TryRead(new Regex("\\([a-zA-Z0-9 _\"]+,[a-zA-Z0-9 _\"()+\\-\\*\\/]+\\)")).Trim();
                        if (temp1.Equals("")) { return false; }
                        if (temp2.Equals("")) { return false; }
                        int count = Regex.Matches(temp2, "\"").Count;
                        if (count != 2 && count != 4) { return false; }
                        if (!parsePattern(temp1, temp2)) { return false; }
                    }
                    else if (textAn.TryRead(RELTYPE_WITH))
                    {
                        textAn.SkipEmptyChars();
                        temp = textAn.TryRead(new Regex("[a-zA-Z0-9\\.]+[a-zA-Z]+#[ =\"]+[a-zA-Z0-9]+[\" ]?"));
                        if (temp==null) { return false; }
                        temp = temp.Trim();
                        if (temp.Equals("")) { return false; }
                        if (Regex.Matches(temp, "=").Count != 1) { return false; }
                        int count = Regex.Matches(temp, "\"").Count;
                        if (count != 0 && count != 2 && count != 4) { return false; }
                        if (!parseWith(temp)) { return false; }
                    }
                    else return false;
                }
            }
            else return false;
            return true;

        }

        private bool parsePattern(string s1, string s2)
        {
            List<string> vars = new List<string>();
            TextAnalyzer tr = new TextAnalyzer();
            tr.Text = s1;
            string args, syn, relation, rel;
            string[] temp;
            syn = tr.TryRead(new Regex("[a-zA-Z0-9]+"));
            if (!isVarNameExists(syn))
                return false;
            vars.Add(syn);
            rel = varMap[syn];

            if (rel == "assign")
            {
                List<string> varTypes = new List<string>(new string[] { "assign", "assign" });
                relation = "PatternAssign";
                tr.Text = s2;
                tr.SkipEmptyChars();
                char[] charsToTrim = { '(', ')' };
                args = tr.Text.Trim(charsToTrim);
                string[] patArgs = new string[2];
                temp = args.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (temp.Length != 2)
                    return false;
                char[] charsToTrim1 = { '\"' };
                for (int i = 0; i < 2; i++)
                {
                    temp[i] = temp[i].Trim();
                    patArgs[i] = temp[i].Trim(charsToTrim1);
                }
                PatternArgs pp = new PatternArgs(patArgs[0], patArgs[1]);
                queryTree.p = pp;
                queryTree.InsertPattern(relation, vars, varTypes);
                return true;
            }
            else if (rel == "if")
            {
                relation = "PatternIf";
                List<string> varTypes = new List<string>(new string[] { "if", "if" });
                tr.Text = s2;
                tr.SkipEmptyChars();
                char[] charsToTrim = { '(', ')' };
                args = tr.Text.Trim(charsToTrim);
                string[] patArgs = new string[2];
                temp = args.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (temp.Length != 2)
                    return false;
                if (temp[1] != "_")
                    return false;
                char[] charsToTrim1 = { '\"' };
                for (int i = 0; i < 2; i++)
                {
                    temp[i] = temp[i].Trim();
                    patArgs[i] = temp[i].Trim(charsToTrim1);
                }
                PatternArgs pp = new PatternArgs(patArgs[0], patArgs[1]);
                queryTree.p = pp;
                queryTree.InsertPattern(relation, vars, varTypes);
                return true;
            }
            else if (rel == "while")
            {
                relation = "PatternWhile";
                List<string> varTypes = new List<string>(new string[] { "while", "while" });
                tr.Text = s2;
                tr.SkipEmptyChars();
                char[] charsToTrim = { '(', ')' };
                args = tr.Text.Trim(charsToTrim);
                string[] patArgs = new string[2];
                temp = args.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (temp.Length != 2)
                    return false;
                if (temp[1] != "_")
                    return false;
                char[] charsToTrim1 = { '\"' };
                for (int i = 0; i < 2; i++)
                {
                    temp[i] = temp[i].Trim();
                    patArgs[i] = temp[i].Trim(charsToTrim1);
                }
                PatternArgs pp = new PatternArgs(patArgs[0], patArgs[1]);
                queryTree.p = pp;
                queryTree.InsertPattern(relation, vars, varTypes);
                return true;
            }
            return false;
        }

        private bool parseWith(string s)
        {
            string arg1 = Regex.Match(s, "^[^= ]+").Value.Trim();
            string arg2 = Regex.Match(s, "[^= ]+$").Value.Trim(new char[] { '"',' ' });
            string syn = Regex.Match(arg1, "[a-zA-Z0-9]+\\.").Value.Trim(new char[] { '.' });
            if (!isVarNameExists(syn)) { return false; }
            List<string> vars = new List<string>();
            List<string> varTypes = new List<string>();
            vars.Add(syn);
            varTypes.Add(getVarType(syn));
            vars.Add(arg2);
            if (!relTable.isArgValid(RELTYPE_WITH_NUMBER, 1, getVarType(syn)) && Regex.IsMatch(arg2, @"^\d+$")) { return false; };
            if (arg1.Contains(VARTYPE_STMT))
            {
                varTypes.Add(VARTYPE_NUMBER);
                queryTree.InsertWith(RELTYPE_WITH_NUMBER, vars, varTypes);
                return true;
            }
            else if (arg1.Contains(VARTYPE_PROCNAME))
            {
                varTypes.Add(VARTYPE_PROCNAME);
                queryTree.InsertWith(RELTYPE_WITH_NAME, vars, varTypes);
                return true;
            }

            return false;
        }

        private bool parseRelationship(string rel)
        {
            TextAnalyzer tr = new TextAnalyzer();
            tr.Text = rel;
            string args, relation;
            string[] temp;
            relation = tr.TryRead(new Regex("[A-Z]{1}[a-z\\*]+"));
            if (!isValidRelationship(relation)) { return false; }
            tr.SkipEmptyChars();
            args = tr.TryRead(new Regex("\\([a-zA-Z0-9 \"]+,[a-zA-Z0-9 \"]+\\)")).Trim(new char[] { '(', ')', ' ' });
            temp = args.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (temp.Length != 2) { return false; }
            List<string> vars = new List<string>(temp);
            List<string> varTypes = new List<string>();
            int tmp;
            for (int i = 0; i < vars.Count; i++)
            {
                vars[i] = vars[i].Trim();
                if (isVarNameExists(vars[i]))
                {
                    if (!relTable.isArgValid(relation, i + 1, getVarType(vars[i])))
                    {
                        return false;
                    }
                    string currVarType = getVarType(vars[i]);
                    if (currVarType.Equals(VARTYPE_PROG_LINE))
                    {
                        varTypes.Add(VARTYPE_STMT);
                    }
                    else
                    {
                        varTypes.Add(currVarType);
                    }
                }
                else if (vars[i].Equals("_"))
                {
                    varTypes.Add(VARTYPE_ALL);
                }
                else if (vars[i].Length > 2 && vars[1].Substring(0, 1).Equals("\"") && vars[1].Substring(vars[i].Length - 1, 1).Equals("\""))
                {
                    if (!relTable.isArgValid(relation, i + 1, VARTYPE_STRING)) { return false; }
                    vars[i] = vars[i].Substring(1, vars[i].Length - 2);
                    varTypes.Add(VARTYPE_STRING);
                }
                else if (Int32.TryParse(vars[i], out tmp))
                {
                    if (!relTable.isArgValid(relation, i + 1, VARTYPE_NUMBER)) { return false; }
                    varTypes.Add(VARTYPE_NUMBER);
                }
            }
            queryTree.InsertSuchThat(relation, vars, varTypes);
            return true;
        }


        public string getVarType(string var)
        {
            return varMap[var];
        }

        private bool isVarNameExists(string varName)
        {
            return varMap.ContainsKey(varName);
        }

        private bool isValidDesignEntity(string entity)
        {
            for (int i = 0; i < designEntites.Length; i++)
            {
                if (designEntites[i].Equals(entity))
                {
                    return true;
                }
            }
            return false;
        }

        private bool isValidRelationship(string rel)
        {
            for (int i = 0; i < relationships.Length; i++)
            {
                if (relationships[i].Equals(rel))
                {
                    return true;
                }
            }
            return false;
        }
    }
}