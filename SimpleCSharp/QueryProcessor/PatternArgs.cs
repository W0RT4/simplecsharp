﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCSharp.QueryProcessor
{
    public class PatternArgs
    {
        //public List<string[]> patArgsList { get; set; } = new List<string[]>();
        public string args1 { get; }
        public string args2 { get; }

        public PatternArgs()
        {

        }

        public PatternArgs(string arg1, string arg2)
        {
            this.args1 = arg1;
            this.args2 = arg2;
        }
    }
}