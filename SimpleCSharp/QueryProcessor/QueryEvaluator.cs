﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleCSharp.Analyser;
using SimpleCSharp.Models.Tree;
using SimpleCSharp.Parser;
using System.IO;
using System.Text.RegularExpressions;

namespace SimpleCSharp.QueryProcessor
{
    class QueryEvaluator
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(typeof(QueryEvaluator));

        private const string MODIFIES_REL = "Modifies";
        private const string USES_REL = "Uses";
        private const string PARENT_REL = "Parent";
        private const string PARENT_STAR_REL = "Parent*";
        private const string FOLLOWS_REL = "Follows";
        private const string FOLLOWS_STAR_REL = "Follows*";
        private const string CALLS_REL = "Calls";
        private const string CALLS_STAR_REL = "Calls*";
        private const string NEXT_REL = "Next";
        private const string NEXT_STAR_REL = "Next*";
        private const string TRUE_STRING = "true";
        private const string FALSE_STRING = "false";
        private const string BOOLEAN_STRING = "BOOLEAN";
        private const string SELECT_TYPE = "Select";
        private const string ASSIGN_TYPE = "assign";
        private const string IF_TYPE = "if";
        private const string WHILE_TYPE = "while";
        private const string PROG_LINE_TYPE = "prog_line";
        private const string STMT_TYPE = "stmt";
        private const string VARIABLE_TYPE = "variable";
        private const string CONSTANT_TYPE = "constant";
        private const string PROCEDURE_TYPE = "procedure";
        private const string CALL_TYPE = "call";
        private const string STRING_TYPE = "string";
        private const string NUMBER_TYPE = "number";
        const string WITH_NAME_REL = "WithName";
        const string WITH_NUMBER_REL = "WithNumber";
        const string VARTYPE_PROCNAME = "procName";
        private const string PATTERN_ASSIGN_REL = "PatternAssign";
        private const string PATTERN_IF_REL = "PatternIf";
        private const string PATTERN_WHILE_REL = "PatternWhile";

        private QueryTree qt;
        private QueryRawResult rawResult;
        private QueryRawResult tmpRes;

        public QueryRawResult getRawResult() { return rawResult; }

        public QueryEvaluator(QueryTree _qt)
        {
            this.qt = _qt;
            rawResult = new QueryRawResult((from o in qt.synonymsList select o.var[0]).ToList());
        }


        private bool updateResult(QueryRawResult qrr)
        {
            QueryRawResult tempRes = new QueryRawResult((from o in qt.synonymsList select o.var[0]).ToList());
            int r; int n;
            if (rawResult.result.Count > 0)
            {

                for (int j = 0; j < qrr.header.Count; j++)
                {
                    for (int i = 0; i < qrr.result.Count; i++)
                    {
                        for (int k = 0; k < rawResult.result.Count; k++)
                        {
                            r = rawResult.result.ElementAt(k)[j];
                            n = qrr.result.ElementAt(i)[j];
                            if (qrr.result.ElementAt(i)[j] != 0 && rawResult.result.ElementAt(k)[j] != 0 && qrr.result.ElementAt(i)[j] == rawResult.result.ElementAt(k)[j])
                            {
                                tempRes.result.Add(qrr.result.ElementAt(i));
                                tempRes.result.Add(rawResult.result.ElementAt(k));
                            }
                        }
                    }
                }
                rawResult = tempRes;
            }
            else
            {
                rawResult = qrr;
            }
            return rawResult.result.Count > 0;
        }

        public void EvaluateQuery()
        {
            for (int i = 0; i < qt.clausesList.Count; i++)
            {
                if (!processClause(qt.clausesList[i]))
                {
                    rawResult.isQueryTrue = false;
                    break;
                }
            }
        }

        private bool processClause(Clause clause)
        {
            bool checker = Regex.Match(clause.relationship, "^Pattern").Success;
            if (checker)
            {
                return processPatternClause(clause);
            }
            else
            {
                tmpRes = new QueryRawResult((from o in qt.synonymsList select o.var[0]).ToList());
                if (clause.relationship.Equals(WITH_NAME_REL))
                {
                    return processWithName(clause);
                }
                else if (clause.relationship.Equals(WITH_NUMBER_REL))
                {
                    return processWithNumber(clause);
                }
                else
                {
                    return processSuchThatClause(clause);
                }
            }
            
        }

        private bool processPatternClause(Clause clause)
        {
            if (clause.relationship.Equals(PATTERN_ASSIGN_REL))
            {
                return processAssign(clause);
            }
            else if (clause.relationship.Equals(PATTERN_IF_REL))
            {
                return processIf(clause);
            }
            else if (clause.relationship.Equals(PATTERN_WHILE_REL))
            {
                return processWhile(clause);
            }
            return false;
        }

        private bool processSuchThatClause(Clause clause)
        {
            if (clause.relationship.Equals(PARENT_REL))
            {
                return processParent(clause);
            }
            else if (clause.relationship.Equals(PARENT_STAR_REL))
            {
                return processParentStar(clause);
            }
            else if (clause.relationship.Equals(NEXT_REL))
            {
                return processNext(clause);
            }
            else if (clause.relationship.Equals(NEXT_STAR_REL))
            {
                return processNextStar(clause);
            }
            else if (clause.relationship.Equals(MODIFIES_REL))
            {
                return processModifies(clause);
            }
            else if (clause.relationship.Equals(USES_REL))
            {
                return processUses(clause);
            }
            else if (clause.relationship.Equals(FOLLOWS_REL))
            {
                return processFollows(clause);
            }
            else if (clause.relationship.Equals(FOLLOWS_STAR_REL))
            {
                return processFollowsStar(clause);
            }
            else if (clause.relationship.Equals(CALLS_REL))
            {
                return processCalls(clause);
            }
            else if (clause.relationship.Equals(CALLS_STAR_REL))
            {
                return processCallsStar(clause);
            }
            return false;
        }

        private bool processAssign(Clause clause)
        {
            string syn = clause.var[0];
            string arg1 = qt.p.args1;
            string arg1Type = clause.varType[0];
            string arg2 = qt.p.args2;
            string arg2Type = clause.varType[1];
            List<Tuple<Element, string>> res = new List<Tuple<Element, string>>();

            if (arg2.Equals("_"))
            {
                res = PKB.getModifies(arg1Type, arg1);
                foreach (var c in res)
                {
                    rawResult.addToResult(syn, c.Item1.Line);
                }
                return res.Count > 0;
            }
            else
            {

                ///

            }
            return false;
        }

        private bool processIf(Clause clause)
        {
            string syn = clause.var[0];
            string arg1 = qt.p.args1;
            string arg1Type = clause.varType[0];
            string arg2 = qt.p.args2;
            string arg2Type = clause.varType[1];
            List<Tuple<Element, string>> res = new List<Tuple<Element, string>>();

            if (arg2.Equals("_"))
            {
                res = PKB.getModifies(arg1Type, arg1);
                foreach (var c in res)
                {
                    rawResult.addToResult(syn, c.Item1.Line);
                }
                return res.Count > 0;
            }
            return false;
        }

        private bool processWhile(Clause clause)
        {
            string syn = clause.var[0];
            string arg1 = qt.p.args1;
            string arg1Type = clause.varType[0];
            string arg2 = qt.p.args2;
            string arg2Type = clause.varType[1];
            List<Tuple<Element, string>> res = new List<Tuple<Element, string>>();

            if (arg2.Equals("_"))
            {
                res = PKB.getModifies(arg1Type, arg1);
                foreach (var c in res)
                {
                    rawResult.addToResult(syn, c.Item1.Line);
                }
                return res.Count > 0;
            }
            return false;
        }

        private bool processParent(Clause clause)
        {
            string arg1 = clause.var[0];
            string arg1Type = clause.varType[0];
            string arg2 = clause.var[1];
            string arg2Type = clause.varType[1];
            List<Tuple<Element, Element>> res = new List<Tuple<Element, Element>>();
            if (arg1Type.Equals("number"))
            {
                if (arg2Type.Equals("number"))
                {
                    res = PKB.GetParents(Int32.Parse(arg1), Int32.Parse(arg2));
                    return res.Count > 0;
                }
                else
                {
                    res = PKB.GetParents(Int32.Parse(arg1), arg2Type);
                    foreach (var c in res)
                    {
                        tmpRes.addToResult(arg2, c.Item2.Line);
                    }
                }
            }
            else
            {
                if (arg2Type.Equals("number"))
                {
                    res = PKB.GetParents(arg1Type, Int32.Parse(arg2));
                    foreach (var c in res)
                    {
                        tmpRes.addToResult(arg1, c.Item1.Line);
                    }
                }
                else
                {
                    res = PKB.GetParents(arg1Type, arg2Type);
                    foreach (var c in res)
                    {
                        tmpRes.addToResult(arg2, c.Item2.Line, arg1, c.Item1.Line);
                    }
                }
            }
            return updateResult(tmpRes);
        }

        private bool processParentStar(Clause clause)
        {
            string arg1 = clause.var[0];
            string arg1Type = clause.varType[0];
            string arg2 = clause.var[1];
            string arg2Type = clause.varType[1];
            List<Tuple<Element, Element>> res = new List<Tuple<Element, Element>>();
            if (arg1Type.Equals("number"))
            {
                if (arg2Type.Equals("number"))
                {
                    res = PKB.GetParents(Int32.Parse(arg1), Int32.Parse(arg2), true);
                    return res.Count > 0;
                }
                else
                {
                    res = PKB.GetParents(Int32.Parse(arg1), arg2Type, true);
                    foreach (var c in res)
                    {
                        tmpRes.addToResult(arg2, c.Item2.Line);
                    }
                }

            }
            else
            {
                if (arg2Type.Equals("number"))
                {
                    res = PKB.GetParents(arg1Type, Int32.Parse(arg2), true);
                    foreach (var c in res)
                    {
                        tmpRes.addToResult(arg1, c.Item1.Line);
                    }
                }
                else
                {
                    res = PKB.GetParents(arg1Type, arg2Type, true);
                    foreach (var c in res)
                    {
                        tmpRes.addToResult(arg2, c.Item2.Line, arg1, c.Item1.Line);
                    }
                }
            }
            return updateResult(tmpRes);
        }
        private bool processNext(Clause clause)
        {
            string arg1 = clause.var[0];
            string arg1Type = clause.varType[0];
            string arg2 = clause.var[1];
            string arg2Type = clause.varType[1];
            List<Tuple<Element, Element>> res = new List<Tuple<Element, Element>>();
            if (arg1Type.Equals("number"))
            {
                if (arg2Type.Equals("number"))
                {
                    res = PKB.GetNexts(Int32.Parse(arg1), Int32.Parse(arg2));
                    return res.Count > 0;
                }
                else
                {
                    res = PKB.GetNexts(Int32.Parse(arg1), arg2Type);
                    foreach (var c in res)
                    {
                        tmpRes.addToResult(arg2, c.Item2.Line);
                    }
                }
            }
            else
            {
                if (arg2Type.Equals("number"))
                {
                    res = PKB.GetNexts(arg1Type, Int32.Parse(arg2));
                    foreach (var c in res)
                    {
                        tmpRes.addToResult(arg1, c.Item1.Line);
                    }
                }
                else
                {
                    res = PKB.GetNexts(arg1Type, arg2Type);
                    foreach (var c in res)
                    {
                        tmpRes.addToResult(arg2, c.Item2.Line, arg1, c.Item1.Line);
                    }
                }
            }
            return updateResult(tmpRes);
        }
        private bool processNextStar(Clause clause)
        {
            string arg1 = clause.var[0];
            string arg1Type = clause.varType[0];
            string arg2 = clause.var[1];
            string arg2Type = clause.varType[1];
            List<Tuple<Element, Element>> res = new List<Tuple<Element, Element>>();
            if (arg1Type.Equals("number"))
            {
                if (arg2Type.Equals("number"))
                {
                    res = PKB.GetNexts(Int32.Parse(arg1), Int32.Parse(arg2), true);
                    return res.Count > 0;
                }
                else
                {
                    res = PKB.GetNexts(Int32.Parse(arg1), arg2Type, true);
                    foreach (var c in res)
                    {
                        tmpRes.addToResult(arg2, c.Item2.Line);
                    }
                }
            }
            else
            {
                if (arg2Type.Equals("number"))
                {
                    res = PKB.GetNexts(arg1Type, Int32.Parse(arg2), true);
                    foreach (var c in res)
                    {
                        tmpRes.addToResult(arg1, c.Item1.Line);
                    }
                }
                else
                {
                    res = PKB.GetNexts(arg1Type, arg2Type, true);
                    foreach (var c in res)
                    {
                        tmpRes.addToResult(arg2, c.Item2.Line, arg1, c.Item1.Line);
                    }
                }
            }
            return updateResult(tmpRes);
        }
        private bool processUses(Clause clause)
        {
            string arg1 = clause.var[0];
            string arg1Type = clause.varType[0];
            string arg2 = clause.var[1];
            string arg2Type = clause.varType[1];
            List<Tuple<Element, string>> res = new List<Tuple<Element, string>>();
            if (arg1Type.Equals("number"))
            {
                if (arg2Type.Equals("string"))
                {
                    return PKB.getUses(Int32.Parse(arg1), arg2);
                }
                else
                {
                    return false; //todo handle others 
                }
            }
            else
            {
                if (arg2Type.Equals("string"))
                {
                    res = PKB.getUses(arg1Type, arg2);
                    foreach (var c in res)
                    {
                        tmpRes.addToResult(arg1, c.Item1.Line);
                    }
                }
                else
                {
                    return false;
                } //todo handle others
            }
            return updateResult(tmpRes);
        }
        private bool processModifies(Clause clause)
        {
            string arg1 = clause.var[0];
            string arg1Type = clause.varType[0];
            string arg2 = clause.var[1];
            string arg2Type = clause.varType[1];
            List<Tuple<Element, string>> res = new List<Tuple<Element, string>>();
            if (arg1Type.Equals("number"))
            {
                if (arg2Type.Equals("string"))
                {
                    return PKB.getModifies(Int32.Parse(arg1), arg2);
                }
                else
                {
                    return false; //todo handle others 
                }
            }
            else
            {
                if (arg2Type.Equals("string"))
                {
                    res = PKB.getModifies(arg1Type, arg2);
                    foreach (var c in res)
                    {
                        tmpRes.addToResult(arg1, c.Item1.Line);
                    }
                }
                else
                {
                    return false; //todo handle others
                }
            }
            return updateResult(tmpRes);
        }

        private bool processFollows(Clause clause)
        {
            string arg1 = clause.var[0];
            string arg1Type = clause.varType[0];
            string arg2 = clause.var[1];
            string arg2Type = clause.varType[1];
            List<Tuple<Element, Element>> res = new List<Tuple<Element, Element>>();
            if (arg1Type.Equals("number"))
            {
                if (arg2Type.Equals("number"))
                {
                    res = PKB.GetFollows(Int32.Parse(arg1), Int32.Parse(arg2));
                    return res.Count > 0;
                }
                else
                {
                    res = PKB.GetFollows(Int32.Parse(arg1), arg2Type);
                    foreach (var c in res)
                    {
                        tmpRes.addToResult(arg2, c.Item2.Line);
                    }
                }
            }
            else
            {
                if (arg2Type.Equals("number"))
                {
                    res = PKB.GetFollows(arg1Type, Int32.Parse(arg2));
                    foreach (var c in res)
                    {
                        tmpRes.addToResult(arg1, c.Item1.Line);
                    }
                }
                else
                {
                    res = PKB.GetFollows(arg1Type, arg2Type);
                    foreach (var c in res)
                    {
                        tmpRes.addToResult(arg2, c.Item2.Line, arg1, c.Item1.Line);
                    }
                }
            }
            return updateResult(tmpRes);
        }

        private bool processFollowsStar(Clause clause)
        {
            string arg1 = clause.var[0];
            string arg1Type = clause.varType[0];
            string arg2 = clause.var[1];
            string arg2Type = clause.varType[1];
            List<Tuple<Element, Element>> res = new List<Tuple<Element, Element>>();
            if (arg1Type.Equals("number"))
            {
                if (arg2Type.Equals("number"))
                {
                    res = PKB.GetFollows(Int32.Parse(arg1), Int32.Parse(arg2), true);
                    return res.Count > 0;
                }
                else
                {
                    res = PKB.GetFollows(Int32.Parse(arg1), arg2Type, true);
                    foreach (var c in res)
                    {
                        tmpRes.addToResult(arg2, c.Item2.Line);
                    }
                }
            }
            else
            {
                if (arg2Type.Equals("number"))
                {
                    res = PKB.GetFollows(arg1Type, Int32.Parse(arg2), true);
                    foreach (var c in res)
                    {
                        tmpRes.addToResult(arg1, c.Item1.Line);
                    }
                }
                else
                {
                    res = PKB.GetFollows(arg1Type, arg2Type, true);
                    foreach (var c in res)
                    {
                        tmpRes.addToResult(arg2, c.Item2.Line, arg1, c.Item1.Line);
                    }
                }
            }
            return updateResult(tmpRes);
        }

        private bool processCalls(Clause clause)
        {
            string arg1 = clause.var[0];
            string arg1Type = clause.varType[0];
            string arg2 = clause.var[1];
            string arg2Type = clause.varType[1];
            List<Tuple<Element, Element>> res = new List<Tuple<Element, Element>>();
            if (arg1Type.Equals("number"))
            {
                if (arg2Type.Equals("number"))
                {
                    res = PKB.GetCalls(Int32.Parse(arg1), Int32.Parse(arg2));
                    return res.Count > 0;
                }
                else
                {
                    res = PKB.GetCalls(Int32.Parse(arg1), arg2Type);
                    foreach (var c in res)
                    {
                        tmpRes.addToResult(arg2, c.Item2.Line);
                    }
                }
            }
            else
            {
                if (arg2Type.Equals("number"))
                {
                    res = PKB.GetCalls(arg1Type, Int32.Parse(arg2));
                    foreach (var c in res)
                    {
                        tmpRes.addToResult(arg1, c.Item1.Line);
                    }
                }
                else
                {
                    res = PKB.GetCalls(arg1Type, arg2Type);
                    foreach (var c in res)
                    {
                        tmpRes.addToResult(arg2, c.Item2.Line, arg1, c.Item1.Line);
                    }
                }
            }
            return updateResult(tmpRes);
        }

        private bool processCallsStar(Clause clause)
        {
            string arg1 = clause.var[0];
            string arg1Type = clause.varType[0];
            string arg2 = clause.var[1];
            string arg2Type = clause.varType[1];
            List<Tuple<Element, Element>> res = new List<Tuple<Element, Element>>();
            if (arg1Type.Equals("number"))
            {
                if (arg2Type.Equals("number"))
                {
                    res = PKB.GetCalls(Int32.Parse(arg1), Int32.Parse(arg2), true);
                    return res.Count > 0;
                }
                else
                {
                    res = PKB.GetCalls(Int32.Parse(arg1), arg2Type, true);
                    foreach (var c in res)
                    {
                        tmpRes.addToResult(arg2, c.Item2.Line);
                    }
                }
            }
            else
            {
                if (arg2Type.Equals("number"))
                {
                    res = PKB.GetCalls(arg1Type, Int32.Parse(arg2), true);
                    foreach (var c in res)
                    {
                        tmpRes.addToResult(arg1, c.Item1.Line);
                    }
                }
                else
                {
                    res = PKB.GetCalls(arg1Type, arg2Type, true);
                    foreach (var c in res)
                    {
                        tmpRes.addToResult(arg2, c.Item2.Line, arg1, c.Item1.Line);
                    }
                }
            }
            return updateResult(tmpRes);
        }
        private bool processWithNumber(Clause clause)
        {
            List<Element> res = new List<Element>();
            string arg1 = clause.var[0];
            string arg1Type = clause.varType[0];
            string arg2 = clause.var[1];
            string arg2Type = clause.varType[1];
            res = PKB.GetEnties(arg1Type);
            foreach (var c in res)
            {
                tmpRes.addToResult(arg1, c.Line);
            }
            updateResult(tmpRes);
            tmpRes = new QueryRawResult((from o in qt.synonymsList select o.var[0]).ToList());
            tmpRes.addToResult(arg1, Int32.Parse(arg2));
            return updateResult(tmpRes);
        }
        private bool processWithName(Clause clause)
        {
            List<Element> res = new List<Element>();
            string arg1 = clause.var[0];
            string arg1Type = clause.varType[0];
            string arg2 = clause.var[1];
            string arg2Type = clause.varType[1];
            if (arg2Type.Equals(VARTYPE_PROCNAME))
            {
                res = PKB.Procedures;
                foreach (var c in res)
                {
                    tmpRes.addToResult(arg1,c.Line);
                }
                updateResult(tmpRes);
                tmpRes = new QueryRawResult((from o in qt.synonymsList select o.var[0]).ToList());
                Element e = PKB.GetProcedure(arg2);
                tmpRes.addToResult(arg1, e!=null?e.Line:-1);
                return updateResult(tmpRes);
            }
            else
            {
                // todo varName
                return false;
            }
         
        }

    }
}
