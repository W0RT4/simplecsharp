﻿using System;
using System.Collections.Generic;
using System.Linq;

using SimpleCSharp.Models.Tree;

namespace SimpleCSharp.Analyser
{
	public partial class Analyser
	{
		private void AnalyzeProcedure(Node procedure)
		{
			this.GetProcedureNexts(procedure);
			this.AnalyzeProcedureChildren(procedure);
			this.GetProcedureModifies((Procedure)procedure);
			this.GetProcedureUsings((Procedure)procedure);
		}

		private void GetProcedureUsings(Procedure procedure)
		{
			var kids = PKB.GetParents(procedure.Line, "stmt", true);

			foreach (var tuple in kids)
			{
				var kidUses = PKB.Uses.Where(n => n.Item1.Line == tuple.Item2.Line).ToList();
				foreach (var kidModify in kidUses)
				{
					var use = new Tuple<Element, string>(new Element(procedure.Line, procedure.Value), kidModify.Item2);
					if (!PKB.Uses.Any(n => n.Item1.Line == use.Item1.Line && n.Item2 == use.Item2))
					{
						PKB.Uses.Add(use);
					}
				}
			}
		}

		private void GetProcedureModifies(Procedure procedure)
		{
			var kids = PKB.GetParents(procedure.Line, "stmt", true);

			foreach (var tuple in kids)
			{
				var kidModifies = PKB.Modifies.Where(n => n.Item1.Line == tuple.Item2.Line).ToList();
				foreach (var kidUse in kidModifies)
				{
					var modifie = new Tuple<Element, string>(new Element(procedure.Line, procedure.Value), kidUse.Item2);
					if (!PKB.Modifies.Any(n => n.Item1.Line == modifie.Item1.Line && n.Item2 == modifie.Item2))
					{
						PKB.Modifies.Add(modifie);
					}
				}
			}
		}

		private void GetProcedureNexts(Node procedure)
		{
			var firstChild = procedure.Children[0].Children[0];
			PKB.Nexts.Add(new Tuple<Element, Element>(new Element(procedure.Line, procedure.Name), new Element(firstChild.Line, firstChild.Name)));
		}

		private void GetFolowsForProcedureChild(Node parent, Node rootChild, int position)
		{
			if (position < parent.Children[0].Children.Count - 1)
			{
				Node secondRootChild = parent.Children[0].Children[position + 1];
				PKB.Follows.Add(new Tuple<Element, Element>(new Element(rootChild.Line, rootChild.Name), new Element(secondRootChild.Line, secondRootChild.Name)));
			}
		}

		private void AnalyzeProcedureChildren(Node procedure)
		{
			for (var position = 0; position < procedure.Children[0].Children.Count; position++)
			{
				Node rootChild = procedure.Children[0].Children[position];

				PKB.Parents.Add(new Tuple<Element, Element>(new Element(procedure.Line, procedure.Name), new Element(rootChild.Line, rootChild.Name)));
				if (rootChild.Name.Equals("assign"))
				{
					Logger.Debug("Line nr. " + rootChild.Line + " is Assignment");
					this.AnalyseAssign(procedure.Children[0], position);
				}
				else if (rootChild.Name.Equals("while"))
				{
					Logger.Debug("Line nr. " + rootChild.Line + " is While");
					this.AnalyseWhile(procedure.Children[0], position);
				}
				else if (rootChild.Name.Equals("if"))
				{
					Logger.Debug("Line nr. " + rootChild.Line + " is if");
					this.AnalyzeIf(procedure.Children[0], position);
				}
				else if (rootChild.Name.Equals("call"))
				{
					Logger.Debug("Line nr. " + rootChild.Line + " is call");
					this.AnalyseCall(procedure.Children[0], position);
				}

				PKB.Enties.Add(new Element(rootChild.Line, rootChild.Name));
				this.GetFolowsForProcedureChild(procedure, rootChild, position);
			}
		}
	}
}
