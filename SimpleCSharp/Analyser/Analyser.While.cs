﻿using System;
using System.Collections.Generic;
using System.Linq;

using SimpleCSharp.Models.Tree;

namespace SimpleCSharp.Analyser
{
	public partial class Analyser
	{
		private void AnalyseWhile(Node parent, int whilePosition)
		{
			var whileNode = parent.Children[whilePosition];

			this.GetWhileNexts(parent, whileNode, whilePosition);
			this.AnalyseWhileChildren(whileNode);

			this.GetWhileUses(whileNode);
			this.GetWhileModifies(whileNode);
		}

		private void GetWhileModifies(Node whileNode)
		{
			var kids = PKB.GetParents(whileNode.Line, "stmt", true);

			foreach (var tuple in kids)
			{
				var kidModifies = PKB.Modifies.Where(n => n.Item1.Line == tuple.Item2.Line).ToList();
				foreach (var kidModify in kidModifies)
				{
					var modifie = new Tuple<Element, string>(new Element(whileNode.Line, whileNode.Name), kidModify.Item2);
					if (!PKB.Modifies.Any(n => n.Item1.Line == modifie.Item1.Line && n.Item2 == modifie.Item2))
					{
						PKB.Modifies.Add(modifie);
					}
				}
			}
		}

		private void GetWhileUses(Node whileNode)
		{
			PKB.Uses.Add(new Tuple<Element, string>(new Element(whileNode.Line, whileNode.Name), ((NameVal)whileNode.Children[0]).Value));

			var kids = PKB.GetParents(whileNode.Line, "stmt", true);

			foreach (var tuple in kids)
			{
				var kidUses = PKB.Uses.Where(n => n.Item1.Line == tuple.Item2.Line).ToList();
				foreach (var kidUse in kidUses)
				{
					var use = new Tuple<Element, string>(new Element(whileNode.Line, whileNode.Name), kidUse.Item2);
					if (!PKB.Uses.Any(n => n.Item1.Line == use.Item1.Line && n.Item2 == use.Item2))
					{
						PKB.Uses.Add(use);
					}
				}
			}
		}

		private void GetWhileNexts(Node parent, Node whileNode, int whilePosition)
		{
			var firstChild = whileNode.Children[1].Children[0];
			PKB.Nexts.Add(new Tuple<Element, Element>(new Element(whileNode.Line, whileNode.Name), new Element(firstChild.Line, firstChild.Name)));

			Node lastWhileChild = whileNode.Children[1].Children[whileNode.Children[1].Children.Count - 1];
			if (lastWhileChild.Name.Equals("assign"))
			{
				this.GetAssignNextToCall(lastWhileChild, whileNode);
			}
			else if (lastWhileChild.Name.Equals("while"))
			{
				this.GetWhileNextToCall(lastWhileChild, whileNode);
			}
			else if (lastWhileChild.Name.Equals("if"))
			{
				this.GetIfNextToCall((If)lastWhileChild, whileNode);
			}
			else if (lastWhileChild.Name.Equals("call"))
			{
				this.GetCallNextToCall((Call)lastWhileChild, whileNode);
			}

			if (whilePosition < parent.Children.Count - 1)
			{
				var next = parent.Children[whilePosition + 1];
				PKB.Nexts.Add(new Tuple<Element, Element>(new Element(whileNode.Line, whileNode.Name), new Element(next.Line, next.Name)));
			}
		}

		private void GetFolowsForWhileChild(Node parent, Node rootChild, int position)
		{
			if (position < parent.Children[1].Children.Count - 1)
			{
				Node secondRootChild = parent.Children[1].Children[position + 1];
				PKB.Follows.Add(new Tuple<Element, Element>(new Element(rootChild.Line, rootChild.Name), new Element(secondRootChild.Line, secondRootChild.Name)));
			}
		}

		private void AnalyseWhileChildren(Node whileNode)
		{
			for (var position = 0; position < whileNode.Children[1].Children.Count; position++)
			{
				Node whileChild = whileNode.Children[1].Children[position];

				PKB.Parents.Add(new Tuple<Element, Element>(new Element(whileNode.Line, whileNode.Name), new Element(whileChild.Line, whileChild.Name)));
				if (whileChild.Name.Equals("assign"))
				{
					this.AnalyseAssign(whileNode.Children[1], position);
				}
				else if (whileChild.Name.Equals("while"))
				{
					this.AnalyseWhile(whileNode.Children[1], position);
				}
				else if (whileChild.Name.Equals("if"))
				{
					this.AnalyzeIf(whileNode.Children[1], position);
				}
				else if (whileChild.Name.Equals("call"))
				{
					this.AnalyseCall(whileNode.Children[1], position);
				}

				PKB.Enties.Add(new Element(whileChild.Line, whileChild.Name));
				this.GetFolowsForWhileChild(whileNode, whileChild, position);
			}
		}

		private void GetWhileNextToCall(Node whileNode, Node next)
		{
			PKB.Nexts.Add(new Tuple<Element, Element>(new Element(whileNode.Line, whileNode.Name), new Element(next.Line, next.Name)));
		}
	}
}
