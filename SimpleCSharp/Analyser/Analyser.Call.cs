﻿using System;
using System.Collections.Generic;
using System.Linq;

using SimpleCSharp.Models.Tree;

namespace SimpleCSharp.Analyser
{
	public partial class Analyser
	{
		private void AnalyseCall(Node parent, int callPosition)
		{
			var callNode = (Call)parent.Children[callPosition];

			this.GetCalls(callNode);
			this.GetCallNexts(parent, callNode, callPosition);
			this.GetCallModifies(callNode);
			this.GetCallUsings(callNode);
		}

		private void GetCallUsings(Call call)
		{
			var procedureUsings = PKB.Uses.Where(n => n.Item1.Name.ToLower().Equals(call.Value.ToLower())).ToList();

			foreach (var tuple in procedureUsings)
			{
				var use = new Tuple<Element, string>(new Element(call.Line, call.Name), tuple.Item2);
				if (!PKB.Uses.Any(n => n.Item1.Line == use.Item1.Line && n.Item2 == use.Item2))
				{
					PKB.Uses.Add(use);
				}
			}
		}

		private void GetCallModifies(Call call)
		{
			var procedureModifies = PKB.Modifies.Where(n => n.Item1.Name.ToLower().Equals(call.Value.ToLower())).ToList();

			foreach (var tuple in procedureModifies)
			{
				var modifie = new Tuple<Element, string>(new Element(call.Line, call.Name), tuple.Item2);
				if (!PKB.Modifies.Any(n => n.Item1.Line == modifie.Item1.Line && n.Item2 == modifie.Item2))
				{
					PKB.Modifies.Add(modifie);
				}
			}
		}


		private void GetCalls(Call callNode)
		{
			var procedure = (Procedure)this.root.Children.First(n => ((Procedure)n).Value.Equals(callNode.Value));

			PKB.Calls.Add(new Tuple<Element, Element>(new Element(this.analyzedProcedure.Line, ((Procedure)this.analyzedProcedure).Value), new Element(procedure.Line, procedure.Value)));
		}

		private void GetCallNexts(Node parent, Call callNode, int callPosition)
		{
			//var firstChildOfCalledProcedure =
			//	this.root.Children.First(n => ((Procedure)n).Value.Equals(callNode.Value)).Children.First();

			//PKB.Nexts.Add(new Tuple<Element, Element>(new Element(callNode.Line, callNode.Name), new Element(firstChildOfCalledProcedure.Line, firstChildOfCalledProcedure.Name)));

			if (callPosition < parent.Children.Count - 1)
			{
				////todo jeżeli next na call to ta medota poza if i zamiast parent call node a tu dajemy next call na parent
				//this.GetNextForInsideCall(callNode, parent.Children[callPosition + 1]);

				PKB.Nexts.Add(new Tuple<Element, Element>(new Element(callNode.Line, callNode.Name), new Element(parent.Children[callPosition + 1].Line, parent.Children[callPosition + 1].Name)));
			}
		}

		private void GetCallNextToCall(Node callNode, Node next)
		{
			PKB.Nexts.Add(new Tuple<Element, Element>(new Element(callNode.Line, callNode.Name), new Element(next.Line, next.Name)));
		}

		//private void GetNextForInsideCall(Call callNode, Node next)
		//{
		//	var lastChildOfCalledProcedure =
		//		this.root.Children.First(n => ((Procedure)n).Value.Equals(callNode.Value)).Children.First().Children.Last();

		//	if (lastChildOfCalledProcedure.Name.Equals("assign"))
		//	{
		//		this.GetAssignNextToCall(lastChildOfCalledProcedure, next);
		//	}
		//	else if (lastChildOfCalledProcedure.Name.Equals("while"))
		//	{
		//		this.GetWhileNextToCall(lastChildOfCalledProcedure, next);
		//	}
		//	else if (lastChildOfCalledProcedure.Name.Equals("if"))
		//	{
		//		this.GetIfNextToCall((If)lastChildOfCalledProcedure, next);
		//	}
		//	else if (lastChildOfCalledProcedure.Name.Equals("call"))
		//	{
		//		this.GetNextForInsideCall((Call)lastChildOfCalledProcedure, next);
		//		//todo jeżeli next na call to tu oddzielna metoda taka sama jak w assign i while
		//	}
		//}
	}
}
