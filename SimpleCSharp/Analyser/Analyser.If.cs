﻿using System;
using System.Collections.Generic;
using System.Linq;

using SimpleCSharp.Models.Tree;

namespace SimpleCSharp.Analyser
{
	public partial class Analyser
	{
		private void AnalyzeIf(Node parent, int ifPosition)
		{
			var ifNode = (If)parent.Children[ifPosition];

			this.GetIfNexts(parent, ifNode, ifPosition);
			this.AnalyseIfChildren(ifNode);

			this.GetIfUses(ifNode);
			this.GetIfModifies(ifNode);
		}

		private void GetIfModifies(If ifNode)
		{
			var kids = PKB.GetParents(ifNode.Line, "stmt", true);

			foreach (var tuple in kids)
			{
				var kidModifies = PKB.Modifies.Where(n => n.Item1.Line == tuple.Item2.Line).ToList();
				foreach (var kidModify in kidModifies)
				{
					var modifie = new Tuple<Element, string>(new Element(ifNode.Line, ifNode.Name), kidModify.Item2);
					if (!PKB.Modifies.Any(n => n.Item1.Line == modifie.Item1.Line && n.Item2 == modifie.Item2))
					{
						PKB.Modifies.Add(modifie);
					}
				}
			}
		}

		private void GetIfUses(If ifNode)
		{
			PKB.Uses.Add(new Tuple<Element, string>(new Element(ifNode.Line, ifNode.Name), ifNode.Condition.Value));

			var kids = PKB.GetParents(ifNode.Line, "stmt", true);

			foreach (var tuple in kids)
			{
				var kidUses = PKB.Uses.Where(n => n.Item1.Line == tuple.Item2.Line).ToList();
				foreach (var kidUse in kidUses)
				{
					var use = new Tuple<Element, string>(new Element(ifNode.Line, ifNode.Name), kidUse.Item2);
					if (!PKB.Uses.Any(n => n.Item1.Line == use.Item1.Line && n.Item2 == use.Item2))
					{
						PKB.Uses.Add(use);
					}
				}
			}
		}

		private void GetIfNexts(Node parent, If ifNode, int ifPosition)
		{
			var firstTrueChild = ifNode.IfTrue.Children.First();
			var firstFalseChild = ifNode.IfFalse?.Children.First();

			var lastTrueChild = ifNode.IfTrue.Children.Last();
			var lastFalseChild = ifNode.IfFalse?.Children.Last();

			PKB.Nexts.Add(new Tuple<Element, Element>(new Element(ifNode.Line, ifNode.Name), new Element(firstTrueChild.Line, firstTrueChild.Name)));
			if (firstFalseChild != null)
			{
				PKB.Nexts.Add(new Tuple<Element, Element>(new Element(ifNode.Line, ifNode.Name), new Element(firstFalseChild.Line, firstFalseChild.Name)));
			}

			if (ifPosition < parent.Children.Count - 1)
			{
				var next = parent.Children[ifPosition + 1];
				if (lastTrueChild.Name.Equals("assign"))
				{
					this.GetAssignNextToCall(lastTrueChild, next);
				}
				else if (lastTrueChild.Name.Equals("while"))
				{
					this.GetWhileNextToCall(lastTrueChild, next);
				}
				else if (lastTrueChild.Name.Equals("if"))
				{
					this.GetIfNextToCall((If)lastTrueChild, next);
				}
				else if (lastTrueChild.Name.Equals("call"))
				{
					this.GetCallNextToCall((Call)lastTrueChild, next);
				}

				if (lastFalseChild != null)
				{
					if (lastFalseChild.Name.Equals("assign"))
					{
						this.GetAssignNextToCall(lastFalseChild, next);
					}
					else if (lastFalseChild.Name.Equals("while"))
					{
						this.GetWhileNextToCall(lastFalseChild, next);
					}
					else if (lastFalseChild.Name.Equals("if"))
					{
						this.GetIfNextToCall((If)lastFalseChild, next);
					}
					else if (lastFalseChild.Name.Equals("call"))
					{
						this.GetCallNextToCall((Call)lastFalseChild, next);
					}
				}
			}
		}

		private void GetFolowsForIfChild(Node parent, Node rootChild, int position)
		{
			if (position < parent.Children.Count - 1)
			{
				Node secondRootChild = parent.Children[position + 1];
				PKB.Follows.Add(new Tuple<Element, Element>(new Element(rootChild.Line, rootChild.Name), new Element(secondRootChild.Line, secondRootChild.Name)));
			}
		}

		private void AnalyseIfChildren(If ifNode)
		{
			for (var position = 0; position < ifNode.IfTrue.Children.Count; position++)
			{
				Node ifChild = ifNode.IfTrue.Children[position];

				PKB.Parents.Add(new Tuple<Element, Element>(new Element(ifNode.Line, ifNode.Name), new Element(ifChild.Line, ifChild.Name)));
				if (ifChild.Name.Equals("assign"))
				{
					this.AnalyseAssign(ifNode.IfTrue, position);
				}
				else if (ifChild.Name.Equals("while"))
				{
					this.AnalyseWhile(ifNode.IfTrue, position);
				}
				else if (ifChild.Name.Equals("if"))
				{
					this.AnalyzeIf(ifNode.IfTrue, position);
				}
				else if (ifChild.Name.Equals("call"))
				{
					this.AnalyseCall(ifNode.IfTrue, position);
				}

				PKB.Enties.Add(new Element(ifChild.Line, ifChild.Name));
				this.GetFolowsForIfChild(ifNode.IfTrue, ifChild, position);
			}

			if (ifNode.IfFalse != null)
			{
				for (var position = 0; position < ifNode.IfFalse.Children.Count; position++)
				{
					Node ifChild = ifNode.IfFalse.Children[position];

					PKB.Parents.Add(new Tuple<Element, Element>(new Element(ifNode.Line, ifNode.Name), new Element(ifChild.Line, ifChild.Name)));
					if (ifChild.Name.Equals("assign"))
					{
						this.AnalyseAssign(ifNode.IfFalse, position);
					}
					else if (ifChild.Name.Equals("while"))
					{
						this.AnalyseWhile(ifNode.IfFalse, position);
					}
					else if (ifChild.Name.Equals("if"))
					{
						this.AnalyzeIf(ifNode.IfFalse, position);
					}
					else if (ifChild.Name.Equals("call"))
					{
						this.AnalyseCall(ifNode.IfFalse, position);
					}

					PKB.Enties.Add(new Element(ifChild.Line, ifChild.Name));
					this.GetFolowsForIfChild(ifNode.IfFalse, ifChild, position);
				}
			}
		}

		private void GetIfNextToCall(If ifNode, Node next)
		{
			var lastTrueIfChild = ifNode.IfTrue.Children.Last();
			if (lastTrueIfChild.Name.Equals("assign"))
			{
				this.GetAssignNextToCall(lastTrueIfChild, next);
			}
			else if (lastTrueIfChild.Name.Equals("while"))
			{
				this.GetWhileNextToCall(lastTrueIfChild, next);
			}
			else if (lastTrueIfChild.Name.Equals("if"))
			{
				this.GetIfNextToCall((If)lastTrueIfChild, next);
			}
			else if (lastTrueIfChild.Name.Equals("call"))
			{
				this.GetCallNextToCall((Call)lastTrueIfChild, next);
			}

			if (ifNode.IfFalse != null)
			{
				var lastFalseIfChild = ifNode.IfFalse.Children.Last();
				if (lastFalseIfChild.Name.Equals("assign"))
				{
					this.GetAssignNextToCall(lastFalseIfChild, next);
				}
				else if (lastFalseIfChild.Name.Equals("while"))
				{
					this.GetWhileNextToCall(lastFalseIfChild, next);
				}
				else if (lastFalseIfChild.Name.Equals("if"))
				{
					this.GetIfNextToCall((If)lastFalseIfChild, next);
				}
				else if (lastFalseIfChild.Name.Equals("call"))
				{
					this.GetCallNextToCall((Call)lastFalseIfChild, next);
				}
			}
		}
	}
}
