﻿using System;
using System.Collections.Generic;
using System.Linq;

using SimpleCSharp.Models.Tree;

namespace SimpleCSharp.Analyser
{
	public partial class Analyser
	{
		/// <summary>
		/// Logger analizatora.
		/// </summary>
		private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(typeof(Analyser));

		/// <summary>
		/// Korzeń drzewa.
		/// </summary>
		private readonly Node root;

		/// <summary>
		/// Analizowana procedura.
		/// </summary>
		private Node analyzedProcedure;

		public Analyser(Node root)
		{
			this.root = root;
		}

		public void AnalyzeTree()
		{
			for (int i = this.root.Children.Count - 1; i >= 0; i--)
			{
				this.analyzedProcedure = this.root.Children[i];

				PKB.Enties.Add(new Element(this.analyzedProcedure.Line, this.analyzedProcedure.Name));
				PKB.Procedures.Add(new Element(this.analyzedProcedure.Line, ((Procedure)this.analyzedProcedure).Value));
				this.AnalyzeProcedure(this.analyzedProcedure);
			}
		}
	}
}
