﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleCSharp.Analyser
{
	public class Element
	{
		public int Line;

		public string Name;

		public Element(int line, string name)
		{
			this.Line = line;
			this.Name = name;
		}
	}
}
