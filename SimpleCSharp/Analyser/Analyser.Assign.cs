﻿using System;
using System.Collections.Generic;
using System.Linq;

using SimpleCSharp.Models.Tree;

namespace SimpleCSharp.Analyser
{
	public partial class Analyser
	{
		private void AnalyseAssign(Node parent, int assignPosition)
		{
			var assign = parent.Children[assignPosition];

			var variable = ((NameVal)assign.Children[0]).Value;
			if (!PKB.Variables.Contains(variable))
			{
				PKB.Variables.Add(variable);
			}

			this.GetAssignNexts(parent, assign, assignPosition);
			this.GetAssignModifies(assign);
			this.GetUses(assign);
		}

		private void GetAssignNexts(Node parent, Node assign, int assignPosition)
		{
			if (assignPosition < parent.Children.Count - 1)
			{
				var next = parent.Children[assignPosition + 1];
				PKB.Nexts.Add(new Tuple<Element, Element>(new Element(assign.Line, assign.Name), new Element(next.Line, next.Name)));
			}
		}

		private void GetAssignModifies(Node assign)
		{
			PKB.Modifies.Add(new Tuple<Element, string>(new Element(assign.Line, assign.Name), ((NameVal)assign.Children[0]).Value));
		}

		private void GetUses(Node assign)
		{
			for (var index = 1; index < assign.Children.Count; index++)
			{
				Node assignChild = assign.Children[index];

				if (assignChild.Name.Equals("factor"))
				{
					PKB.Uses.Add(new Tuple<Element, string>(new Element(assign.Line, assign.Name), ((FactorVal)assignChild).Value));
				}
				else
				{
					this.AnalyseExp(assign, assignChild.Children.ToList());
				}
			}
		}

		private void GetAssignNextToCall(Node assign, Node next)
		{
			PKB.Nexts.Add(new Tuple<Element, Element>(new Element(assign.Line, assign.Name), new Element(next.Line, next.Name)));
		}
	}
}
