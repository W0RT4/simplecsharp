﻿using SimpleCSharp.Analyser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCSharp
{
	public static class PKB
	{
		/// <summary>
		/// Lista Next
		/// Pierwszy to linia po której następuje i co to jest
		/// Drugi to linia która następuje po pierwszej i co to jest
		/// </summary>
		public static List<Tuple<Element, Element>> Nexts = new List<Tuple<Element, Element>>();

		/// <summary>
		/// Lista Parent
		/// Pierwszy to rodzic i co to jest
		/// Drugi to dziecko i co to jest
		/// </summary>
		public static List<Tuple<Element, Element>> Parents = new List<Tuple<Element, Element>>();

		/// <summary>
		/// Lista Modifies
		/// Pierwszy to linia w której zachodzi modyfikacja i co to jest
		/// Drugi to element modyfikowany
		/// </summary>
		public static List<Tuple<Element, string>> Modifies = new List<Tuple<Element, string>>();

		/// <summary>
		/// Lista Uses
		/// Pierwszy to linia w której zachodzi użycie i co to jest
		/// Drugi to element wykorzystywany
		/// </summary>
		public static List<Tuple<Element, string>> Uses = new List<Tuple<Element, string>>();

		/// <summary>
		/// Lista Follows
		/// Pierwszy to rodzic i co to jest
		/// Drugi to dziecko i co to jest
		/// </summary>
		public static List<Tuple<Element, Element>> Follows = new List<Tuple<Element, Element>>();

		/// <summary>
		/// Lista Calls
		/// Pierwszy to rodzic i co to jest
		/// Drugi to dziecko i co to jest
		/// </summary>
		public static List<Tuple<Element, Element>> Calls = new List<Tuple<Element, Element>>();

		/// <summary>
		/// Lista encji.
		/// </summary>
		public static List<Element> Enties = new List<Element>();

		/// <summary>
		/// Lista procedur
		/// </summary>
		public static List<Element> Procedures = new List<Element>();

		/// <summary>
		/// Lista procedur
		/// </summary>
		public static List<string> Variables = new List<string>();

        private static List<string> _keywords = new List<string>() { "procedure", "stmt", "call", "while", "if", "else", "assign", };

		#region getNexts
		public static List<Tuple<Element, Element>> GetNexts(string stmt1, string stmt2, bool indirect = false)
		{
			List<Tuple<Element, Element>> outList = new List<Tuple<Element, Element>>();

			if (stmt1.Equals("stmt") && stmt2.Equals("stmt"))
				return Nexts;

			if (!indirect)
			{
				if (stmt1.Equals("stmt"))
					outList = Nexts.Where(x => x.Item2.Name == stmt2).ToList();
				else if (stmt2.Equals("stmt"))
					outList = Nexts.Where(x => x.Item1.Name.Equals(stmt1)).ToList();
				else
					outList = Nexts.Where(x => x.Item1.Name.Equals(stmt1) && x.Item2.Name.Equals(stmt2)).ToList();
				return outList;
			}

			if (stmt1.Equals("stmt"))
				outList = Nexts.Where(x => x.Item2.Name == stmt2).ToList();
			else if (stmt2.Equals("stmt"))
				outList = Nexts.Where(x => x.Item1.Name.Equals(stmt1)).ToList();
			else
				outList = Nexts.Where(x => x.Item1.Name.Equals(stmt1) && x.Item2.Name.Equals(stmt2)).ToList();

			var tempList = Nexts.Where(x => x.Item1.Name == stmt1).ToList();
			var tempList2 = new List<Tuple<Element, Element>>();
			//outList.AddRange(tempList.Where(x => x.Item2.Name == stmt2));

			while (tempList.Count() > 0 || tempList2.Count() > 0)
			{
				foreach (var tuple in tempList)
				{
					var temp = Nexts.Where(x => x.Item1.Name == tuple.Item2.Name && x.Item1.Line == tuple.Item2.Line);
					outList.AddRange(temp.Where(x => x.Item2.Name == stmt2));
					tempList2.AddRange(temp);
				}
				tempList.Clear();

				foreach (var tuple in tempList2)
				{
					var temp = Nexts.Where(x => x.Item1.Name == tuple.Item2.Name && x.Item1.Line == tuple.Item2.Line);
					outList.AddRange(temp.Where(x => x.Item2.Name == stmt2));
					tempList.AddRange(temp);
				}
				tempList2.Clear();
			}
			return outList;
		}

		public static List<Tuple<Element, Element>> GetNexts(string stmt, int line, bool indirect = false)
		{
			List<Tuple<Element, Element>> outList = new List<Tuple<Element, Element>>();

			if (!indirect)
			{
				if (stmt.Equals("stmt"))
					outList = Nexts.Where(x => x.Item2.Line == line).ToList();
				else
					outList = Nexts.Where(x => x.Item1.Name == stmt && x.Item2.Line == line).ToList();
				return outList;
			}

			if (stmt.Equals("stmt"))
				outList = Nexts.Where(x => x.Item2.Line == line).ToList();
			else
				outList = Nexts.Where(x => x.Item1.Name == stmt && x.Item2.Line == line).ToList();


			var tempList = Nexts.Where(x => x.Item1.Name == stmt).ToList();
			var tempList2 = new List<Tuple<Element, Element>>();

			while (tempList.Count() > 0 || tempList2.Count() > 0)
			{
				foreach (var tuple in tempList)
				{
					var temp = Nexts.Where(x => x.Item1.Name == tuple.Item2.Name && x.Item1.Line == tuple.Item2.Line);
					outList.AddRange(temp.Where(x => x.Item2.Line == line));
					tempList2.AddRange(temp);
				}
				tempList.Clear();

				foreach (var tuple in tempList2)
				{
					var temp = Nexts.Where(x => x.Item1.Name == tuple.Item2.Name && x.Item1.Line == tuple.Item2.Line);
					outList.AddRange(temp.Where(x => x.Item2.Line == line));
					tempList.AddRange(temp);
				}
				tempList2.Clear();
			}

			return outList;
		}

		public static List<Tuple<Element, Element>> GetNexts(int line, string stmt, bool indirect = false)
		{
			List<Tuple<Element, Element>> outList = new List<Tuple<Element, Element>>();

			if (!indirect)
			{
				if (stmt.Equals("stmt"))
					outList = Nexts.Where(x => x.Item1.Line == line).ToList();
				else
					outList = Nexts.Where(x => x.Item1.Line == line && x.Item2.Name == stmt).ToList();
				return outList;
			}

			if (stmt.Equals("stmt"))
				outList = Nexts.Where(x => x.Item1.Line == line).ToList();
			else
				outList = Nexts.Where(x => x.Item1.Line == line && x.Item2.Name == stmt).ToList();


			var tempList = Nexts.Where(x => x.Item1.Line == line).ToList();
			var tempList2 = new List<Tuple<Element, Element>>();

			while (tempList.Count() > 0 || tempList2.Count() > 0)
			{
				foreach (var tuple in tempList)
				{
					var temp = Nexts.Where(x => x.Item1.Name == tuple.Item2.Name && x.Item1.Line == tuple.Item2.Line);
					outList.AddRange(temp.Where(x => x.Item2.Name == stmt));
					tempList2.AddRange(temp);
				}
				tempList.Clear();

				foreach (var tuple in tempList2)
				{
					var temp = Nexts.Where(x => x.Item1.Name == tuple.Item2.Name && x.Item1.Line == tuple.Item2.Line);
					outList.AddRange(temp.Where(x => x.Item2.Name == stmt));
					tempList.AddRange(temp);
				}
				tempList2.Clear();
			}

			return outList;
		}

		public static List<Tuple<Element, Element>> GetNexts(int line1, int line2, bool indirect = false)
		{
			List<Tuple<Element, Element>> outList = new List<Tuple<Element, Element>>();

			if (!indirect)
			{
				outList = Nexts.Where(x => x.Item1.Line.Equals(line1) && x.Item2.Line.Equals(line2)).ToList();
				return outList;
			}

			outList = Nexts.Where(x => x.Item1.Line.Equals(line1) && x.Item2.Line.Equals(line2)).ToList();

			var tempList = Nexts.Where(x => x.Item1.Line == line1).ToList();
			var tempList2 = new List<Tuple<Element, Element>>();
			//outList.AddRange(tempList.Where(x => x.Item2.Name == stmt2));

			while (tempList.Count() > 0 || tempList2.Count() > 0)
			{
				foreach (var tuple in tempList)
				{
					var temp = Nexts.Where(x => x.Item1.Name == tuple.Item2.Name && x.Item1.Line == tuple.Item2.Line);
					outList.AddRange(temp.Where(x => x.Item2.Line == line2));
					tempList2.AddRange(temp);
				}
				tempList.Clear();

				foreach (var tuple in tempList2)
				{
					var temp = Nexts.Where(x => x.Item1.Name == tuple.Item2.Name && x.Item1.Line == tuple.Item2.Line);
					outList.AddRange(temp.Where(x => x.Item2.Line == line2));
					tempList.AddRange(temp);
				}
				tempList2.Clear();
			}

			return outList;
		}
		#endregion

		#region getParents

		public static List<Tuple<Element, Element>> GetParents(int line, string stmt, bool indirect = false)
		{
			List<Tuple<Element, Element>> outList = new List<Tuple<Element, Element>>();

			if (!indirect)
			{
				if (stmt.Equals("stmt"))
					outList = Parents.Where(x => x.Item1.Line == line).ToList();
				else
					outList = Parents.Where(x => x.Item1.Line == line && x.Item2.Name == stmt).ToList();
				return outList;
			}

			if (stmt.Equals("stmt"))
				outList = Parents.Where(x => x.Item1.Line == line).ToList();
			else
				outList = Parents.Where(x => x.Item1.Line == line && x.Item2.Name == stmt).ToList();

			var tempList = Parents.Where(x => x.Item1.Line == line).ToList();
			var tempList2 = new List<Tuple<Element, Element>>();
			//outList.AddRange(tempList.Where(x => x.Item2.Name == stmt2));

			while (tempList.Count() > 0 || tempList2.Count() > 0)
			{
				foreach (var tuple in tempList)
				{
					var temp = Parents.Where(x => x.Item1.Name.Equals(tuple.Item2.Name) && x.Item1.Line.Equals(tuple.Item2.Line)).ToList();

					outList.AddRange(temp.Where(x => x.Item2.Name == stmt));
					tempList2.AddRange(temp);
				}
				tempList.Clear();

				foreach (var tuple in tempList2)
				{
					var temp = new List<Tuple<Element, Element>>();
					temp = Parents.Where(x => x.Item1.Name.Equals(tuple.Item2.Name) && x.Item1.Line.Equals(tuple.Item2.Line)).ToList();
					outList.AddRange(temp.Where(x => x.Item2.Name == stmt));
					tempList.AddRange(temp);
				}
				tempList2.Clear();
			}
			return outList;
		}

		public static List<Tuple<Element, Element>> GetParents(string stmt, int line, bool indirect = false)
		{
			List<Tuple<Element, Element>> outList = new List<Tuple<Element, Element>>();

			if (!indirect)
			{
				if (stmt.Equals("stmt"))
					outList = Parents.Where(x => x.Item2.Line == line).ToList();
				else
					outList = Parents.Where(x => x.Item1.Name == stmt && x.Item2.Line == line).ToList();
				return outList;
			}

			if (stmt.Equals("stmt"))
				outList = Parents.Where(x => x.Item2.Line == line).ToList();
			else
				outList = Parents.Where(x => x.Item1.Name == stmt && x.Item2.Line == line).ToList();

			var tempList = Parents.Where(x => x.Item1.Name == stmt).ToList();
			var tempList2 = new List<Tuple<Element, Element>>();
			//outList.AddRange(tempList.Where(x => x.Item2.Name == stmt2));

			while (tempList.Count() > 0 || tempList2.Count() > 0)
			{
				foreach (var tuple in tempList)
				{
					var temp = Parents.Where(x => x.Item1.Name.Equals(tuple.Item2.Name) && x.Item1.Line.Equals(tuple.Item2.Line)).ToList();
					outList.AddRange(temp.Where(x => x.Item2.Line == line));
					tempList2.AddRange(temp);
				}
				tempList.Clear();

				foreach (var tuple in tempList2)
				{
					var temp = new List<Tuple<Element, Element>>();
					temp = Parents.Where(x => x.Item1.Name.Equals(tuple.Item2.Name) && x.Item1.Line.Equals(tuple.Item2.Line)).ToList();
					outList.AddRange(temp.Where(x => x.Item2.Line == line));
					tempList.AddRange(temp);
				}
				tempList2.Clear();
			}
			return outList;
		}

		public static List<Tuple<Element, Element>> GetParents(string stmt1, string stmt2, bool indirect = false)
		{
			List<Tuple<Element, Element>> outList = new List<Tuple<Element, Element>>();

			if (stmt1.Equals("stmt") && stmt2.Equals("stmt"))
				return Parents;

			if (!indirect)
			{
				if (stmt1.Equals("stmt"))
					outList = Parents.Where(x => x.Item2.Name == stmt2).ToList();
				else if (stmt2.Equals("stmt"))
					outList = Parents.Where(x => x.Item1.Name.Equals(stmt1)).ToList();
				else
					outList = Parents.Where(x => x.Item1.Name.Equals(stmt1) && x.Item2.Name.Equals(stmt2)).ToList();
				return outList;
			}

			if (stmt1.Equals("stmt"))
				outList = Parents.Where(x => x.Item2.Name == stmt2).ToList();
			else if (stmt2.Equals("stmt"))
				outList = Parents.Where(x => x.Item1.Name.Equals(stmt1)).ToList();
			else
				outList = Parents.Where(x => x.Item1.Name.Equals(stmt1) && x.Item2.Name.Equals(stmt2)).ToList();

			var tempList = Parents.Where(x => x.Item1.Name == stmt1).ToList();
			var tempList2 = new List<Tuple<Element, Element>>();
			//outList.AddRange(tempList.Where(x => x.Item2.Name == stmt2));

			while (tempList.Count() > 0 || tempList2.Count() > 0)
			{
				foreach (var tuple in tempList)
				{
					var temp = Parents.Where(x => x.Item1.Name.Equals(tuple.Item2.Name) && x.Item1.Line.Equals(tuple.Item2.Line)).ToList();

					outList.AddRange(temp.Where(x => x.Item2.Name == stmt2));
					tempList2.AddRange(temp);
				}
				tempList.Clear();

				foreach (var tuple in tempList2)
				{
					var temp = new List<Tuple<Element, Element>>();
					temp = Parents.Where(x => x.Item1.Name.Equals(tuple.Item2.Name) && x.Item1.Line.Equals(tuple.Item2.Line)).ToList();
					outList.AddRange(temp.Where(x => x.Item2.Name == stmt2));
					tempList.AddRange(temp);
				}
				tempList2.Clear();
			}
			return outList;
		}

		public static List<Tuple<Element, Element>> GetParents(int line1, int line2, bool indirect = false)
		{
			List<Tuple<Element, Element>> outList = new List<Tuple<Element, Element>>();

			outList = Parents.Where(x => x.Item1.Line.Equals(line1) && x.Item2.Line.Equals(line2)).ToList();

			var tempList = Parents.Where(x => x.Item1.Line == line1).ToList();
			var tempList2 = new List<Tuple<Element, Element>>();
			//outList.AddRange(tempList.Where(x => x.Item2.Name == stmt2));

			while (tempList.Count() > 0 || tempList2.Count() > 0)
			{
				foreach (var tuple in tempList)
				{
					var temp = new List<Tuple<Element, Element>>();
					temp = Parents.Where(x => x.Item1.Name.Equals(tuple.Item2.Name) && x.Item1.Line.Equals(tuple.Item2.Line)).ToList();
					outList.AddRange(temp.Where(x => x.Item2.Line == line2));
					tempList2.AddRange(temp);
				}
				tempList.Clear();

				foreach (var tuple in tempList2)
				{
					var temp = new List<Tuple<Element, Element>>();

					temp = Parents.Where(x => x.Item1.Name.Equals(tuple.Item2.Name) && x.Item1.Line.Equals(tuple.Item2.Line)).ToList();

					outList.AddRange(temp.Where(x => x.Item2.Line == line2));
					tempList.AddRange(temp);
				}
				tempList2.Clear();
			}
			return outList;
		}

		#endregion

		#region getModifies

		public static List<Tuple<Element, string>> getModifies(string stmt, string var = null)
		{
			List<Tuple<Element, string>> outList = new List<Tuple<Element, string>>();

            if (String.IsNullOrWhiteSpace(var))
            {
                if(stmt == "procedure")
                    outList = Modifies.Where(x => !_keywords.Contains(x.Item1.Name)).ToList();
                else if (stmt.Equals("stmt"))
                    outList = Modifies.ToList();
                else
                    outList = Modifies.Where(x => x.Item1.Name.Equals(stmt)).ToList();
            }
            else
            {
                if (stmt == "procedure")
                    outList = Modifies.Where(x => !_keywords.Contains(x.Item1.Name)).ToList();
                else if (stmt.Equals("stmt"))
                    outList = Modifies.Where(x => x.Item2 == var).ToList();
                else
                    outList = Modifies.Where(x => x.Item1.Name.Equals(stmt) && x.Item2 == var).ToList();
            }

			return outList;
		}

		public static bool getModifies(int line, string var)
		{
			return Modifies.Where(x => x.Item1.Line == line && x.Item2 == var).Select(n => n.Item2).ToList().Count > 0 ? true : false;
		}

		#endregion

		#region getUses

		public static List<Tuple<Element, string>> getUses(string stmt, string var = null)
		{
			List<Tuple<Element, string>> outList = new List<Tuple<Element, string>>();

            if (String.IsNullOrWhiteSpace(var))
            {
                if (stmt == "procedure")
                    outList = Uses.Where(x => !_keywords.Contains(x.Item1.Name)).ToList();
                else if (stmt.Equals("stmt"))
                    outList = Uses.ToList();
                else
                    outList = Uses.Where(x => x.Item1.Name.Equals(stmt)).ToList();
            }
            else
            {
                if (stmt == "procedure")
                    outList = Uses.Where(x => !_keywords.Contains(x.Item1.Name)).ToList();
                else if (stmt.Equals("stmt"))
                    outList = Uses.Where(x => x.Item2 == var).ToList();
                else
                    outList = Uses.Where(x => x.Item1.Name.Equals(stmt) && x.Item2 == var).ToList();
            }

			return outList;
		}

		public static bool getUses(int line, string var)
		{
			return Uses.Where(x => x.Item1.Line == line && x.Item2 == var).Select(n => n.Item2).ToList().Count > 0 ? true : false;
		}

		#endregion

		#region getFollows
		public static List<Tuple<Element, Element>> GetFollows(string stmt1, string stmt2, bool indirect = false)
		{
			List<Tuple<Element, Element>> outList = new List<Tuple<Element, Element>>();

			if (stmt1.Equals("stmt") && stmt2.Equals("stmt"))
				return Follows;

			if (!indirect)
			{
				if (stmt1.Equals("stmt"))
					outList = Follows.Where(x => x.Item2.Name == stmt2).ToList();
				else if (stmt2.Equals("stmt"))
					outList = Follows.Where(x => x.Item1.Name.Equals(stmt1)).ToList();
				else
					outList = Follows.Where(x => x.Item1.Name.Equals(stmt1) && x.Item2.Name.Equals(stmt2)).ToList();
				return outList;
			}

			if (stmt1.Equals("stmt"))
				outList = Follows.Where(x => x.Item2.Name == stmt2).ToList();
			else if (stmt2.Equals("stmt"))
				outList = Follows.Where(x => x.Item1.Name.Equals(stmt1)).ToList();
			else
				outList = Follows.Where(x => x.Item1.Name.Equals(stmt1) && x.Item2.Name.Equals(stmt2)).ToList();

			var tempList = Follows.Where(x => x.Item1.Name == stmt1).ToList();
			var tempList2 = new List<Tuple<Element, Element>>();
			//outList.AddRange(tempList.Where(x => x.Item2.Name == stmt2));

			while (tempList.Count() > 0 || tempList2.Count() > 0)
			{
				foreach (var tuple in tempList)
				{
					var temp = Follows.Where(x => x.Item1.Name == tuple.Item2.Name && x.Item1.Line == tuple.Item2.Line);
					outList.AddRange(temp.Where(x => x.Item2.Name == stmt2));
					tempList2.AddRange(temp);
				}
				tempList.Clear();

				foreach (var tuple in tempList2)
				{
					var temp = Follows.Where(x => x.Item1.Name == tuple.Item2.Name && x.Item1.Line == tuple.Item2.Line);
					outList.AddRange(temp.Where(x => x.Item2.Name == stmt2));
					tempList.AddRange(temp);
				}
				tempList2.Clear();
			}
			return outList;
		}

		public static List<Tuple<Element, Element>> GetFollows(string stmt, int line, bool indirect = false)
		{
			List<Tuple<Element, Element>> outList = new List<Tuple<Element, Element>>();

			if (!indirect)
			{
				if (stmt.Equals("stmt"))
					outList = Follows.Where(x => x.Item2.Line == line).ToList();
				else
					outList = Follows.Where(x => x.Item1.Name == stmt && x.Item2.Line == line).ToList();
				return outList;
			}

			if (stmt.Equals("stmt"))
				outList = Follows.Where(x => x.Item2.Line == line).ToList();
			else
				outList = Follows.Where(x => x.Item1.Name == stmt && x.Item2.Line == line).ToList();


			var tempList = Follows.Where(x => x.Item1.Name == stmt).ToList();
			var tempList2 = new List<Tuple<Element, Element>>();

			while (tempList.Count() > 0 || tempList2.Count() > 0)
			{
				foreach (var tuple in tempList)
				{
					var temp = Follows.Where(x => x.Item1.Name == tuple.Item2.Name && x.Item1.Line == tuple.Item2.Line);
					outList.AddRange(temp.Where(x => x.Item2.Line == line));
					tempList2.AddRange(temp);
				}
				tempList.Clear();

				foreach (var tuple in tempList2)
				{
					var temp = Follows.Where(x => x.Item1.Name == tuple.Item2.Name && x.Item1.Line == tuple.Item2.Line);
					outList.AddRange(temp.Where(x => x.Item2.Line == line));
					tempList.AddRange(temp);
				}
				tempList2.Clear();
			}

			return outList;
		}

		public static List<Tuple<Element, Element>> GetFollows(int line, string stmt, bool indirect = false)
		{
			List<Tuple<Element, Element>> outList = new List<Tuple<Element, Element>>();

			if (!indirect)
			{
				if (stmt.Equals("stmt"))
					outList = Follows.Where(x => x.Item1.Line == line).ToList();
				else
					outList = Follows.Where(x => x.Item1.Line == line && x.Item2.Name == stmt).ToList();
				return outList;
			}

			if (stmt.Equals("stmt"))
				outList = Follows.Where(x => x.Item1.Line == line).ToList();
			else
				outList = Follows.Where(x => x.Item1.Line == line && x.Item2.Name == stmt).ToList();


			var tempList = Follows.Where(x => x.Item1.Line == line).ToList();
			var tempList2 = new List<Tuple<Element, Element>>();

			while (tempList.Count() > 0 || tempList2.Count() > 0)
			{
				foreach (var tuple in tempList)
				{
					var temp = Follows.Where(x => x.Item1.Name == tuple.Item2.Name && x.Item1.Line == tuple.Item2.Line);
					outList.AddRange(temp.Where(x => x.Item2.Name == stmt));
					tempList2.AddRange(temp);
				}
				tempList.Clear();

				foreach (var tuple in tempList2)
				{
					var temp = Follows.Where(x => x.Item1.Name == tuple.Item2.Name && x.Item1.Line == tuple.Item2.Line);
					outList.AddRange(temp.Where(x => x.Item2.Name == stmt));
					tempList.AddRange(temp);
				}
				tempList2.Clear();
			}

			return outList;
		}

		public static List<Tuple<Element, Element>> GetFollows(int line1, int line2, bool indirect = false)
		{
			List<Tuple<Element, Element>> outList = new List<Tuple<Element, Element>>();

			if (!indirect)
			{
				outList = Follows.Where(x => x.Item1.Line.Equals(line1) && x.Item2.Line.Equals(line2)).ToList();
				return outList;
			}

			outList = Follows.Where(x => x.Item1.Line.Equals(line1) && x.Item2.Line.Equals(line2)).ToList();

			var tempList = Follows.Where(x => x.Item1.Line == line1).ToList();
			var tempList2 = new List<Tuple<Element, Element>>();
			//outList.AddRange(tempList.Where(x => x.Item2.Name == stmt2));

			while (tempList.Count() > 0 || tempList2.Count() > 0)
			{
				foreach (var tuple in tempList)
				{
					var temp = Follows.Where(x => x.Item1.Name == tuple.Item2.Name && x.Item1.Line == tuple.Item2.Line);
					outList.AddRange(temp.Where(x => x.Item2.Line == line2));
					tempList2.AddRange(temp);
				}
				tempList.Clear();

				foreach (var tuple in tempList2)
				{
					var temp = Follows.Where(x => x.Item1.Name == tuple.Item2.Name && x.Item1.Line == tuple.Item2.Line);
					outList.AddRange(temp.Where(x => x.Item2.Line == line2));
					tempList.AddRange(temp);
				}
				tempList2.Clear();
			}

			return outList;
		}

		#endregion

		#region getCalls
		public static List<Tuple<Element, Element>> GetCalls(string stmt1, string stmt2, bool indirect = false)
		{
			List<Tuple<Element, Element>> outList = new List<Tuple<Element, Element>>();

			if (stmt1.Equals("stmt") && stmt2.Equals("stmt"))
				return Calls;

			if (!indirect)
			{
				if (stmt1.Equals("stmt"))
					outList = Calls.Where(x => x.Item2.Name == stmt2).ToList();
				else if (stmt2.Equals("stmt"))
					outList = Calls.Where(x => x.Item1.Name.Equals(stmt1)).ToList();
				else
					outList = Calls.Where(x => x.Item1.Name.Equals(stmt1) && x.Item2.Name.Equals(stmt2)).ToList();
				return outList;
			}

			if (stmt1.Equals("stmt"))
				outList = Calls.Where(x => x.Item2.Name == stmt2).ToList();
			else if (stmt2.Equals("stmt"))
				outList = Calls.Where(x => x.Item1.Name.Equals(stmt1)).ToList();
			else
				outList = Calls.Where(x => x.Item1.Name.Equals(stmt1) && x.Item2.Name.Equals(stmt2)).ToList();

			var tempList = Calls.Where(x => x.Item1.Name == stmt1).ToList();
			var tempList2 = new List<Tuple<Element, Element>>();
			//outList.AddRange(tempList.Where(x => x.Item2.Name == stmt2));

			while (tempList.Count() > 0 || tempList2.Count() > 0)
			{
				foreach (var tuple in tempList)
				{
					var temp = Calls.Where(x => x.Item1.Name == tuple.Item2.Name && x.Item1.Line == tuple.Item2.Line);
					outList.AddRange(temp.Where(x => x.Item2.Name == stmt2));
					tempList2.AddRange(temp);
				}
				tempList.Clear();

				foreach (var tuple in tempList2)
				{
					var temp = Calls.Where(x => x.Item1.Name == tuple.Item2.Name && x.Item1.Line == tuple.Item2.Line);
					outList.AddRange(temp.Where(x => x.Item2.Name == stmt2));
					tempList.AddRange(temp);
				}
				tempList2.Clear();
			}
			return outList;
		}

		public static List<Tuple<Element, Element>> GetCalls(string stmt, int line, bool indirect = false)
		{
			List<Tuple<Element, Element>> outList = new List<Tuple<Element, Element>>();

			if (!indirect)
			{
				if (stmt.Equals("stmt"))
					outList = Calls.Where(x => x.Item2.Line == line).ToList();
				else
					outList = Calls.Where(x => x.Item1.Name == stmt && x.Item2.Line == line).ToList();
				return outList;
			}

			if (stmt.Equals("stmt"))
				outList = Calls.Where(x => x.Item2.Line == line).ToList();
			else
				outList = Calls.Where(x => x.Item1.Name == stmt && x.Item2.Line == line).ToList();


			var tempList = Calls.Where(x => x.Item1.Name == stmt).ToList();
			var tempList2 = new List<Tuple<Element, Element>>();

			while (tempList.Count() > 0 || tempList2.Count() > 0)
			{
				foreach (var tuple in tempList)
				{
					var temp = Calls.Where(x => x.Item1.Name == tuple.Item2.Name && x.Item1.Line == tuple.Item2.Line);
					outList.AddRange(temp.Where(x => x.Item2.Line == line));
					tempList2.AddRange(temp);
				}
				tempList.Clear();

				foreach (var tuple in tempList2)
				{
					var temp = Calls.Where(x => x.Item1.Name == tuple.Item2.Name && x.Item1.Line == tuple.Item2.Line);
					outList.AddRange(temp.Where(x => x.Item2.Line == line));
					tempList.AddRange(temp);
				}
				tempList2.Clear();
			}

			return outList;
		}

		public static List<Tuple<Element, Element>> GetCalls(int line, string stmt, bool indirect = false)
		{
			List<Tuple<Element, Element>> outList = new List<Tuple<Element, Element>>();

			if (!indirect)
			{
				if (stmt.Equals("stmt"))
					outList = Calls.Where(x => x.Item1.Line == line).ToList();
				else
					outList = Calls.Where(x => x.Item1.Line == line && x.Item2.Name == stmt).ToList();
				return outList;
			}

			if (stmt.Equals("stmt"))
				outList = Calls.Where(x => x.Item1.Line == line).ToList();
			else
				outList = Calls.Where(x => x.Item1.Line == line && x.Item2.Name == stmt).ToList();


			var tempList = Calls.Where(x => x.Item1.Line == line).ToList();
			var tempList2 = new List<Tuple<Element, Element>>();

			while (tempList.Count() > 0 || tempList2.Count() > 0)
			{
				foreach (var tuple in tempList)
				{
					var temp = Calls.Where(x => x.Item1.Name == tuple.Item2.Name && x.Item1.Line == tuple.Item2.Line);
					outList.AddRange(temp.Where(x => x.Item2.Name == stmt));
					tempList2.AddRange(temp);
				}
				tempList.Clear();

				foreach (var tuple in tempList2)
				{
					var temp = Calls.Where(x => x.Item1.Name == tuple.Item2.Name && x.Item1.Line == tuple.Item2.Line);
					outList.AddRange(temp.Where(x => x.Item2.Name == stmt));
					tempList.AddRange(temp);
				}
				tempList2.Clear();
			}

			return outList;
		}

		public static List<Tuple<Element, Element>> GetCalls(int line1, int line2, bool indirect = false)
		{
			List<Tuple<Element, Element>> outList = new List<Tuple<Element, Element>>();

			if (!indirect)
			{
				outList = Calls.Where(x => x.Item1.Line.Equals(line1) && x.Item2.Line.Equals(line2)).ToList();
				return outList;
			}

			outList = Calls.Where(x => x.Item1.Line.Equals(line1) && x.Item2.Line.Equals(line2)).ToList();

			var tempList = Calls.Where(x => x.Item1.Line == line1).ToList();
			var tempList2 = new List<Tuple<Element, Element>>();
			//outList.AddRange(tempList.Where(x => x.Item2.Name == stmt2));

			while (tempList.Count() > 0 || tempList2.Count() > 0)
			{
				foreach (var tuple in tempList)
				{
					var temp = Calls.Where(x => x.Item1.Name == tuple.Item2.Name && x.Item1.Line == tuple.Item2.Line);
					outList.AddRange(temp.Where(x => x.Item2.Line == line2));
					tempList2.AddRange(temp);
				}
				tempList.Clear();

				foreach (var tuple in tempList2)
				{
					var temp = Calls.Where(x => x.Item1.Name == tuple.Item2.Name && x.Item1.Line == tuple.Item2.Line);
					outList.AddRange(temp.Where(x => x.Item2.Line == line2));
					tempList.AddRange(temp);
				}
				tempList2.Clear();
			}

			return outList;
		}

		#endregion

		#region getEnties

		public static List<Element> GetEnties(string stmt)
		{
			if (stmt.ToLower().Equals("stmt"))
			{
				return Enties;
			}

			return Enties.Where(n => n.Name.Equals(stmt.ToLower())).ToList();
		}

		#endregion

		#region getProcedure
		public static Element GetProcedure(string name)
		{
			return Procedures.FirstOrDefault(n => n.Name.ToLower().Equals(name.ToLower()));
		}

		public static string GetProcedure(int line)
		{
			return Procedures.FirstOrDefault(n => n.Line == line)?.Name;
		}

		#endregion

		#region getVariable

		public static int GetVariable(string v)
		{
			return Variables.IndexOf(v);
		}

		public static string GetVariable(int index)
		{
			return Variables[index];
		}

		#endregion
	}
}
