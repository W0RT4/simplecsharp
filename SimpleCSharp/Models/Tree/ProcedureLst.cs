﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleCSharp.Models.Tree
{
    public class ProcedureLst : Node
    {
        public override string Name => "procedureLst";

        public ProcedureLst() : base()
        {

        }

        public ProcedureLst(int line, int position) : base(line, position)
        {

        }
    }
}
