﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleCSharp.Models.Tree
{
    class Assign : Stmt
    {
        public override string Name => "assign";

        public NameVal NameVal
        {
            get => this.Children[0] as NameVal;
            set => this.Children[0] = value;
        }
        public Node Exp
        {
            get => this.Children[1] as NameVal;
            set => this.Children[1] = value;
        }

        public Assign(int stmtNo) : base(stmtNo)
        {
            this.Children.Add(null);
            this.Children.Add(null);
        }

        public Assign(int line, int position, int stmtNo) : base(line, position, stmtNo)
        {
            this.Children.Add(null);
            this.Children.Add(null);
        }
    }
}
