﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleCSharp.Models.Tree
{
    public abstract class Stmt : Node
    {

        /// <summary>
        /// statmenet no in procedure
        /// </summary>
        public int StmtNo { get; set; }

        protected Stmt(int stmtNo) : base()
        {
            this.StmtNo = stmtNo;
        }
        protected Stmt(int line, int position, int stmtNo) : base(line, position)
        {
            this.StmtNo = stmtNo;
        }

        public override string ToString()
        {
            return this.Name + " #" + StmtNo;
        }
    }
}
