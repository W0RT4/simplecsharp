﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleCSharp.Models.Tree
{
    class If : Stmt
    {
        public override string Name => "if";

        public NameVal Condition
        {
            get
            {
                return this.Children[0] as NameVal;
            }
            set => this.Children[0] = value;
        }
        public Node IfTrue { get; set; }
        public Node IfFalse { get; set; }

        public If(int stmtNo) : base(stmtNo)
        {
            this.Children.Add(null);
            this.Children.Add(null);
            this.Children.Add(null);
        }

        public If(int line, int position, int stmtNo) : base(line, position, stmtNo)
        {
            this.Children.Add(null);
            this.Children.Add(null);
            this.Children.Add(null);
        }
    }
}
