﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleCSharp.Models.Tree
{
    class FactorVal : Node
    {
        public override string Name => "factor";

        public string Value { get; set; }

        public FactorVal() : base()
        {

        }

        public FactorVal(int line, int position) : base(line, position)
        {

        }
    }
}
