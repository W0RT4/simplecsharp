﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleCSharp.Models.Tree
{
    class While : Stmt
    {
        public override string Name => "while";

        public While(int stmtNo) : base(stmtNo)
        {
        }

        public While(int line, int position, int stmtNo) : base(line, position, stmtNo)
        {

        }
    }
}
