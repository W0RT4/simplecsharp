﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleCSharp.Models.Tree
{
    public class StmtLst : Node
    {
        public override string Name => "stmtLst";

        public StmtLst() : base()
        {

        }

        public StmtLst(int line, int position) : base(line, position)
        {

        }
    }
}
