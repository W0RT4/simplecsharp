﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleCSharp.Exceptions
{
	public class InvalidExpressionException : ParserException
	{
		public InvalidExpressionException(int position, int line) : base("Invalid expression on position: " + position + " (line: " + line + ")")
		{
		}
	}
}
