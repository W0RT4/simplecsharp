﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleCSharp.Exceptions
{
	public class StmtLstExpectedException : ParserException
	{
		public StmtLstExpectedException(int position, int line) : base("Expected: StmtLstExpectedException on position: " + position + " (line: " + line + ")")
		{
		}
	}
}
