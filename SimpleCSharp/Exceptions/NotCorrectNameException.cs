﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleCSharp.Exceptions
{
	public class NotCorrectNameException : ParserException
	{
		public NotCorrectNameException(int position, int line) : base("Not correct name on position: " + position + " (line: " + line + ")")
		{
		}
	}
}
